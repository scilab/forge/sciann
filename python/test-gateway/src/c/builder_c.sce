// ====================================================================
// Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

//tbx_build_src(['snnsversion'], ['snnsversion.c'], 'c', ..
//              get_absolute_file_path('builder_c.sce') );
tbx_build_src(['snnsversion'], ['snnsversion.c'], 'c', ..
              get_absolute_file_path('builder_c.sce'), ['/usr/lib/libflex', '/usr/lib/libsnns'], '-lm -lflex -lsnns', '-I/home/ron/code/scilab/scilab/bin/snns/src/c/kernel/sources/ I/home/ron/code/scilab/scilab/bin/snns/src/c/ -I../.. I./');
              //get_absolute_file_path('builder_c.sce'), ['./libkernel.a', './libfunc.a', './libfl.a'], '-lm', '-I/scilab-5.1/share/scilab/contrib/snns/src/c/kernel/sources/');
              //get_absolute_file_path('builder_c.sce'), [], '-lm');

clear tbx_build_src;


// tbx_build_gateway('sci_coinor', list_add_inter, files_to_compile, get_absolute_file_path('builder_gateway_cpp.sce'), [], ldflags, cflags);
// ./kernel/sources/libkernel.a ./kernel/sources/libfunc.a -lm -lfl

tbx_build_gateway('snns_c', ..
                  [ ..
                    'snns_version','sci_snnsversion'; ..
                    'snns_loadnetwork','sci_loadnetwork'; ..
                    'snns_runnetwork','sci_runnetwork'; ..
                    'ann_version','sci_ann_get_version'; ..
                  ], ..
                  ['sci_snnsversion.c', ..
                   'helper.c', ..
                   ], ..
                  get_absolute_file_path('builder_gateway_c.sce'), ..
                  ['../../src/c/libsnnsversion'], '-lm -lflex -lsnns', '-I/home/ron/code/scilab/scilab/bin/snns/src/c/kernel/sources/ -I/home/ron/code/scilab/scilab/bin/snns/sci_gateway/c/ -I../.. -I./');

clear tbx_build_gateway;

/* ==================================================================== */
/* Allan CORNET */
/* INRIA 2008 */
/* Toolbox SNNS */
/* This file is released into the public domain */ /* ==================================================================== */
#include "stack-c.h" 

#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "api_variable.h"

/* ==================================================================== */
/* ==================================================================== */

#include <stdio.h>
#include <string.h>

#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"

#include "glob_typ.h"
#include "kr_ui.h"

#include "helper.h"

extern void snnsversion( char *outstring );
extern void loadnetworkSNNS(void);
extern void runnetworkSNNS(void);


int sci_snnsversion( char *fname )
{
    int one = 1;
    int string_size = 256;
    int string;

    CheckRhs(0,0) ;
    CheckLhs(1,1) ;   
  
    CreateVar( 1, STRING_DATATYPE, &one, &string_size, &string );

    snnsversion( cstk( string ) );

    LhsVar( 1 ) = 1;  

    return 0;
}
/* ==================================================================== */

int sci_ann_get_version(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;// = "truc";
    //printf("0\n");

    /*** Check for number of parameters ***/
    CheckRhs(0, 0);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    //var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    //krm_putNet(var_network);

    //printf("1\n");

    // *** Function call
    sci = krui_getVersion();

    /*** Hook functions end ***/
    //krm_getNet(var_network);

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(1, row_sci_ret, col_sci_ret, strings_sci_ret);

    //printf("2\n");
    /*** Set return parameters on stack ***/
    LhsVar(1) = 1;

    //printf("3\n");
    /*** Postrequisites return ***/
    free(strings_sci_ret);

    //printf("4\n");

    return 0;
}



/*
int sci_loadnetwork( char *fname )
{
    int one = 1;
    int string_size = 256;
    int string;
    memNet *ann = (memNet *) malloc(sizeof(memNet));

    CheckRhs(0,0) ;
    CheckLhs(1,1) ;   
  
    CreateVar( 1, STRING_DATATYPE, &one, &string_size, &string );

//    snnsversion( cstk( string ) );
    strcpy( cstk( string ), krui_getVersion() );
    strcpy( cstk( string ), "save net " );
    
    krm_getNet( ann ); 
    krm_putNet( ann ); 
    krui_loadNet( "/tmp/letters.net", cstk(string) );
    krui_saveNet( "/tmp/network.net", "essai_network" );
    strcpy( cstk( string ), "bim bam boom" );

    LhsVar( 1 ) = 1;  

    return 0;
}

*/


int sci_loadnetwork( char *fname )
{
    int one = 1;
    int res;
    memNet *ann = (memNet *) malloc(sizeof(memNet));

    CheckRhs(0,0) ;
    CheckLhs(1,1) ;   

    loadnetworkSNNS();
    krm_getNet( ann ); 

    res = ctos_memNet(ann, 1);
    if (res) return res;
    if ( ann == NULL )
    {
        Scierror(999,"Problem while creating the fann scilab structure\n");
        return 0;
    }

    //krm_putNet( ann ); 
    //runnetworkSNNS();

    LhsVar(1) = 1;
    return 0;
}


int sci_runnetwork( char *fname )
{
    int one = 1;
    int res;
    memNet *ann = NULL;// = (memNet *) malloc(sizeof(memNet));

    if( Rhs != 1 )
    {
        Scierror(999,"Wrong number of arguments\n");
        return 0;
    }

    //CheckRhs(1,2) ;
    //CheckLhs(0,0) ;   

    printf("sci_runnetwork - nb of args %d\n", Rhs);

    ann = stoc_memNet(1, &res);
    if (res) return res;
    if ( ann == NULL )
    {
        Scierror(999,"Problem while creating the fann scilab structure\n");
        return 0;
    }

    printf("struct ok\n");

    krm_putNet( ann ); 
    runnetworkSNNS();
    free(ann);

    return 0;
}

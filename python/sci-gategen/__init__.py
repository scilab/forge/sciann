"""
sci-gategen - Gateway generator for Scilab
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/

__all__ = []


%(top)s

int getMatrixFromDoubleTo_%(name_type_escaped)s(int* _piAddress, int* _piRows, int* _piCols, %(name_type)s** _pdblReal)
{
    int i, ret;
    %(name_type)s * table = (%(name_type)s *) malloc(*_piRows * *_piCols * sizeof(%(name_type)s));
    
    ret = getMatrixOfDouble(_piAddress, _piRows, _piCols, (double**)_pdblReal);
    if(ret) return ret;

    /* Save from the double addressing to the %(name_type)s addressing */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        table[i] = (%(name_type)s) (*(double**)_pdblReal)[i];

    /* Copying back the int addressing into what should have been the  addressing, using the same memory space */
    for(i = 0; i < *_piRows * *_piCols; ++i)
        (*_pdblReal)[i] = table[i];

    free(table); 
}


int createMatrixOfDoubleFrom_%(name_type_escaped)s(int _iVar, int _piRows, int _piCols, %(name_type)s* _pdblReal)
{
    int i, ret;
    double * table = (double *) malloc(_piRows * _piCols * sizeof(double));
    
    /* Save from the int addressing to the double addressing */
    for(i = 0; i < _piRows * _piCols; ++i)
        table[i] = (double) _pdblReal[i];

    ret = createMatrixOfDouble(_iVar, _piRows, _piCols, table);
    free(table); 
    if(ret) return ret;
}

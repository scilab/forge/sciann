table =           [ ..
%(list_gateways)s
                  ];


// do not modify below
// Workaround for the weird limit of 100 func%(path)sons
for i = 0:((size(table,1)-1)/99)
    
    // Split table in tables with 100 elements
    t = table(1 + 99 * i:min(size(table,1),99*(i+1)),:);
    
    tbx_build_gateway('%(name_toolbox)s_' + string(i), t, ..
                  [ ..
%(list_files)s
                  ], ..
                  %(path)s, ..
                  [%(libraries)s], '%(links)s', '%(includes)s');

    clear tbx_build_gateway;

    // Copy loader and cleaner
    copyfile(%(path)s + "/loader.sce", ...
                %(path)s + "/loader_" + string(i) + ".sce");
                
    copyfile(%(path)s + "/cleaner.sce", ...
                %(path)s + "/cleaner_" + string(i) + ".sce");

end;

// Create a wrapper for the new loader and cleaner.
fl = mopen(%(path)s + "/loader.sce", "w");
fc = mopen(%(path)s + "/cleaner.sce", "w");
for i = 0:((size(table,1)-1)/100)
    mfprintf(fl, "exec " + %(path)s + "loader_" + string(i) + ".sce;\n");
    mfprintf(fc, "exec " + %(path)s + "cleaner_" + string(i) + ".sce;\n");
end;
mclose(fl);
mclose(fc);

clear fl;
clear fc;
clear table;
clear t;

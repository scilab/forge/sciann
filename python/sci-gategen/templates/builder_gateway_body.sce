tbx_build_gateway('%(name_toolbox)s', ..
                  [ ..
%(list_gateways)s
                  ], ..
                  [ ..
%(list_files)s
                  ], ..
                  %(path)s, ..
                  [%(libraries)s], '%(links)s', '%(includes)s');

clear tbx_build_gateway;

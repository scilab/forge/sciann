%(top)s

int ctos_%(name_type_escaped)s(unsigned int StackPos, %(name_type)s *type)
{
    int m_list_labels,  n_list_labels;
    int m_type_pointer, n_type_pointer;
    int m_extra,        n_extra,   l_extra;

    //The type field names
    static char *fieldnames[] = {"%(name_type_escaped)slist","%(name_type_escaped)s"};

    m_type_pointer = 1;
    n_type_pointer = 1;

    m_extra        = 2;
    n_extra        = 1;

    CreateVar(StackPos, "m", &m_extra,  &n_extra, &l_extra);
  
    m_list_labels = 2;
    n_list_labels = 1;

    CreateListVarFromPtr(StackPos, POS_LABELS,         "S", &m_list_labels,  &n_list_labels,  fieldnames);   // Put the type and the labels
    CreateListVarFromPtr(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer, &n_type_pointer, type);

    return 0;
}

%(name_type)s * stoc_%(name_type_escaped)s(unsigned int StackPos, int * res)
{
    int m_type_pointer, n_type_pointer, l_type_pointer;
    int m_param,        n_param,        l_param;
    int m_label,        n_label;
    char ** LabelList;
    %(name_type)s * result_type = NULL;

    GetRhsVar(StackPos, "m", &m_param, &n_param, &l_param);
    GetListRhsVar(StackPos, POS_LABELS, "S", &m_label, &n_label, &LabelList);

    if (strcmp(LabelList[0],"%(name_type_escaped)slist") != 0) 
    {
      Scierror(999,"Argument 1 is not a %(name_type_escaped)slist\r\n");
    }

    GetListRhsVar(StackPos, POS_STRUCT_POINTER, "p", &m_type_pointer,  &n_type_pointer,  &l_type_pointer);

    result_type = (%(name_type)s *)((unsigned long)*stk(l_type_pointer));

    *res = 0;

    return result_type;
}



"""
Function module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/



class FunctionParameter:
    """
    Stores the necessary information regarding a function parameter.

    :IVariables:
        self.name_type : string
            Type for the parameter. 
        self.name_parameter : string
            Name of the parameter.
        self.position : integer
            Position of the parameter in the prototype.
    """

    def __init__(self, name_type, name_parameter, position):
        """
        Initializer.

        :Parameters:
            name_type : string
                Type for the parameter. 
            name_parameter : string
                Name of the parameter.
            position : integer
                Position of the parameter in the prototype.
        """
        self.name_type = name_type
        self.name_parameter = name_parameter
        self.position = position



class FunctionPrototype:
    """
    Stores function prototypes from source languages.

    :IVariables:
        self.name_function : string
            Name of the function of which the prototype is being handled.
        self.name_class : string
            Name of the class to which the pre-cited function belongs to.
            Equals to None if the language does not support classes.
        self.parameters : sequence of `FunctionParameter`
            Store the parameters of the current function prototype.
        self.parameter_return : `FunctionParameter`
            Store the return value of the current function prototype.
    """

    def __init__(self, name_class=None, name_function=None, parameter_return=None, comments=None):
        """
        Initializer.
        """
        self.name_class = name_class
        self.name_function = name_function
        self.name_function_lower = ''
        self.name_function_new = ''
        self.parameter_return = parameter_return
        self.parameters = []
        self.comments = comments


    def add_parameter(self, parameter):
        self.parameters.append(parameter)


    def get_strings_parameters(self):
        for parameter in self.parameters:
            yield parameter.name_type
            yield parameter.name_parameter

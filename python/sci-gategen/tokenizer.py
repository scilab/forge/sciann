"""
Tokenizer module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/

import re

from function import FunctionParameter
from function import FunctionPrototype
from gateway import GatewayPrototype
from gateway import GatewayParameter


class Tokenizer:
    """
    Abstract class for the language tokenizers.
    """

    def __init__(self):
        """
        Initializer.
        """
        pass


    def strings_prototype(self, string):
        """
        Look for function prototypes in a given string.

        :Parameters:
            string : string
                string in which to find function prototypes.
                
        :Returns: 
            A generator to the substrings containing function prototypes.
        """
        pass


    def prototype_string_to_object(self, string):
        """
        Build a `FunctionPrototype` from the given string

        :Parameters:
            string : string
                string from which to build the `FunctionPrototype`.

        :Returns:
            `FunctionPrototype` : The function prototype built from the given string.
        """
        pass


    def string_function_call(gw_prototype):
        """
        Build a string to call the function encapusalated into the
        `GatewayPrototype` instance given in parameter.

        :Parameters:
            gw_prototype : GatewayPrototype
                Prototype for which a call has to be made.

        :Returns:
            string : the function call for the given `GatewayPrototype`.
        """
        pass



class TokenizerCLang(Tokenizer):
    """
    Tokenizer for C language
    """

    names_type = ['int', 'long', 'short', 'float', 'double', 'char', 'void', 'unsigned']
    names_non_type_keywords = ['static']
    names_type_numeric_non_double = ['int', 'long', 'short', 'char', 'unsigned', 'float']

    def __init__(self):
        """
        Initializer.
        """
        Tokenizer.__init__(self)
        # TODO what if we have extern _AND_ static?
            #(?P<comments>//.*?|/\*.*?\*/)*?
        self.regex_c_prototype = re.compile(
            r"""
            [\s]*?
            (?P<comments>//.*?(\r\n?|\n)|/\*.*?\*/)*?
            [\s]*?
            (register|extern|static)? 
            (?P<return>([ ]*?\w+)+?             # function macros and return type
                ([ ]*?(\*|\[[ ]*?\])+?)*?)      # function pointer characters '*'
            [ ]*?(?P<name>\w+)[ ]*?             # function name
            \(                                  # parameter list starts
                (?P<parameters>[\s\,\w\*\[\]\(\)]*)    # parameter list
            \)                                  # parameter list ends
            (\s|//.*?|/\*.*?\*/)*?              # comments and spaces
            \s*?[;{]                            # end of prototype 
            [\s]*?
            """, re.VERBOSE | re.DOTALL)
                #(\s*?\w+?)*?\s*?       # --- parameter type
                #(\s*?\*+?)*?\s*?       # --- parameter pointer characters '*'
                #\w+?\s*?\,?\s*?        # --- parameter name


    # TODO this heuristic is incomplete, since it doesn't handle
    # the fact that the return type can be on a line before the
    # function prototype.
    # hope i'll remember how this stuff is working...
    def __strings_ft(self, strings_raw):
        string_valid = ''
        string_raw_previous = ''
        in_function = False
        in_comment = False
        in_comment_previous = False
        in_comment_inner = False # to skip comments between ) and {
        nb_level = 0

        for string_raw in strings_raw:

            string_raw_stripped = string_raw.strip()

            # TODO: delele print
            #if 'resetNet' in string_raw:
            #    print 'bingo'

            # if nothing or comment, just loop in order to keep
            # the states of boolean variables
            if not string_raw_stripped or string_raw_stripped.startswith('//'):
                continue

            # entering in a comment inner sequence
            if in_function and string_raw_stripped.startswith('/*'):
                in_comment_inner = True

            # continuing a comment inner sequence
            if in_comment_inner:
                if '*/' in string_raw:
                    # getting out of a comment inner sequence
                    in_comment_inner = False
                continue

            if in_function or in_comment:
                string_valid += string_raw
            elif '(' in string_raw:
                if nb_level == 0:
                    # avoiding taking parenthesis inside functions,
                    # we check that the level is currently 0
                    if in_comment_previous:
                        # a comment was here before, we just
                        # add the current string to the valid one
                        string_valid += string_raw
                        in_comment_previous = False
                    else:
                        # no comment, then we just replace the string
                        string_valid = string_raw
                    in_function = True
            elif '/*' in string_raw:
                # a comment has been detected
                string_valid = string_raw
                in_comment = True
            
            if in_comment and '*/' in string_raw:
                # end of a comment, but that also means that for the next
                # turn of the loop, there was a comment before
                in_comment = False
                in_comment_previous = True

            if in_function \
              and (')' in string_raw or ')' in string_raw_previous) \
              and (';' in string_raw or '{' in string_raw):
                # this is the end of a function prototype 
                in_function = False
                in_comment = False
                in_comment_previous = False
                yield string_valid

            # handling level
            if '{' in string_raw:
                nb_level += 1

            if '}' in string_raw:
                nb_level -= 1

            # whatever happens, functions are closed if ')' was on
            # the previous line; that helps avoiding the misdetection
            # of parenthesis that do not belong to a function prototype
            if ')' not in string_raw and ')' in string_raw_previous:
                in_function = False

            string_raw_previous = string_raw


    def count_pointers_in_string(self, string):
        return len([True for char in string if char in '*['])


    # TODO also handle [ and ] ?
    def get_type_in_string(self, string):
        return string.replace('*', '')


    def is_name_type_valid(self, name_type, names_type=names_type):
        words = re.split('[\W]+', self.get_type_in_string(name_type))
        if all([word in names_type for word in words]):
            return True
        else:
            return False


    def is_conversion_to_double_needed(self, name_type):
        return self.is_name_type_valid(name_type, self.names_type_numeric_non_double)


    def string_to_parameter(self, string, index_parameter=None):
        string = string.strip()
        nb_pointers = self.count_pointers_in_string(string)

        # This test reject void but not void*
        if string and not (string.startswith('void') and nb_pointers == 0):
            # get rid of the last words if it is empty
            words = re.split('[\W]+', string)
            if len(words[-1]) == 0:
                words = words[:-1]

            pattern = re.compile('(%s)' % '|'.join(self.names_non_type_keywords))
            type = '%s%s' % (pattern.sub('', ' '.join(words[:-1])).strip(), '*' * nb_pointers)
            name = words[-1]
            parameter = FunctionParameter(type, name, index_parameter)
        else:
            parameter = None

        return parameter


    def strings_prototype(self, strings):

        for string in self.__strings_ft(strings):
            match = self.regex_c_prototype.match(string)
            if match != None:
                #print match.group()
                #print match.group('return')
                #print match.group('name')
                #yield match.group()
                yield string


    def string_to_prototype(self, string):
        # for one string, use the regex already present to pull out
        # the parameters, and then, build the prototype objematch.group('parameters')ct

        match = self.regex_c_prototype.match(string)

        # Skip the function pointers in parameters
        #print match.group('parameters')
        if '(' in match.group('parameters'):
            return None

        name_function = match.group('name').strip()
        #print 'name function:', name_function
        comments = match.group('comments').strip() if match.group('comments') else ''

        # TODO making reference to the C language, and therefore shouldn't be here.
        # since in C return parameters don't have name, we simply give
        # it the name 'sci_ret'
        string_return = match.group('return').strip() + ' ' + 'sci'
        parameter_return = self.string_to_parameter(string_return)

        prototype = FunctionPrototype(None, name_function, parameter_return, comments)

        #print '|',match.group('parameters').strip(),'|'
        for index_parameter, string_parameter in enumerate(match.group('parameters').split(',')):
            index_parameter += 1
            parameter = self.string_to_parameter(string_parameter, index_parameter)
            prototype.add_parameter(parameter)
        #print '-'*50

        return prototype


    # TODO shoudn't be here
    # TODO fix this function using string format "string % {'template': value}"
    def string_function_call(self, gw_prototype):
        call = ''
        #prefixes = {0: '*', 1: '', 2: '&'}
        #call += '%(prefix)s%(name)s = ' % {'prefix': prefixes.get(gw_parameter.nb_pointers, ''), 'name': gw_parameter.name_parameter_new}
        
        # Handles return value
        gw_parameter = gw_prototype.get_parameter_return_source()
        if gw_parameter != None:
            call += '%(name)s = ' % {'name': gw_parameter.name_parameter_new}
            #call += '*stk(ptr_' + gw_parameter.name_parameter + ') = '

        # Handles function call
        parameters = ['%(name)s' % {'name': gw_parameter.name_parameter_new} for gw_parameter in gw_prototype.get_parameters_source()]
        call += gw_prototype.name_function + '(' + ', '.join(parameters) + ');'
        return call


    # TODO shoudn't be here
    def string_variable_definition(self, gw_parameter):
        return '%(type)s %(name)s;' % {'type': gw_parameter.name_type, 'name': gw_parameter.name_parameter_new}

"""
Generator module.
"""
__docformat__ = "restructuredtext en"

## Emmanuel Goossaert 
## Scilab 2009
##
## This file is part of sci-gategen.
##
## sci-gategen is free software; you can redistribute it and/or modify
## it under the terms of the CeCILL Free Software License Agreement.
## To obtain a copy of the license, or for further information, please
## visit: http://www.scilab.org/

import re
import sys
import os
import logging


import rule

from function import FunctionParameter
from function import FunctionPrototype
from gateway import GatewayParameter
from gateway import GatewayPrototype
from gateway import GatewayFile
from tokenizer import TokenizerCLang
from ruleio import RuleIO_File
from rule import RuleSet
from gatewayio import GatewayIO_Scilab52

logger = logging.getLogger(os.path.basename(sys.argv[0]))


class Generator:
    """
    Handle the whole gateway generating process.
    """

    def __init__(self, tokenizer=None, gw_writer=None):
        """
        Initializer.
        """
        self.strings_author = ''
        self.string_name_toolbox = ''
        self.tokenizer = tokenizer
        self.gw_writer = gw_writer
        self.paths_directory_input = []
        self.paths_file_input = []
        self.ft_prototypes = []
        self.gw_prototypes = []
        self.gw_files = {}
        self.rule_set = None
        self.names_type_valid = []
        self.names_type_user = []
        self.authors = []
        self.compiler_libraries = []
        self.compiler_links = []
        self.compiler_includes = []
        self.name_project = 'project'
        self.language = 'c'
        self.name_file_default = 'gateway.c'
        self.headers = []


    def load_rule_file(self, name_file):
        ruleio = RuleIO_File(name_file)
        self.rule_set = RuleSet()
        ruleio.read(self.rule_set)
        #self.rule_set.print_dict()


    def filter_rules(self):
        for rule_current in self.rule_set.get_rules_all():
            pass


    def fill_field(self, name_rule, field_to_fill):
        for rule_field in self.rule_set.get_rules_by_type(name_rule):
            setattr(self, field_to_fill, rule_field.value)
            break


    def fill_fields(self, name_rule, list_to_fill):
        for rule_field in self.rule_set.get_rules_by_type(name_rule):
            list_to_fill.append(rule_field.value)


    def fill_configuration_parameters(self):
        self.fill_field('RuleFieldLanguage', 'language')
        self.fill_field('RuleFieldProject', 'name_project')
        self.fill_fields('RuleFieldAuthor', self.authors)
        self.fill_fields('RuleFieldLibrary', self.compiler_libraries)
        self.fill_fields('RuleFieldLink', self.compiler_links)
        self.fill_fields('RuleFieldInclude', self.compiler_includes)


    def fill_directories(self):
        #self.path_directory_output = None
        paths_directory_input = []
        for rule_directory in self.rule_set.get_rules_by_type('RuleDirectoryPermission'):
            if rule_directory.action == 'input':
                paths_directory_input.append(rule_directory.name_directory)
            #elif not self.path_directory_output: # action == 'output'
            #    pass

        paths_directory_input_loop = paths_directory_input if paths_directory_input else ['./']

        #if not self.path_directory_output:
        #    self.path_directory_output = self.path_directory_output_default
        #    logger.warning('No output directory given, using default: "%s"' % self.path_directory_output)

        while paths_directory_input_loop:
            path_directory = paths_directory_input_loop.pop()
            if os.path.exists(path_directory):
                paths_directory_sub = [os.path.join(path_directory, f) for f in os.listdir(path_directory) if os.path.isdir(os.path.join(path_directory, f))]
                # Add the new directories for the current loop,
                paths_directory_input_loop.extend(paths_directory_sub)
                # Save the popped directory
                self.paths_directory_input.append(path_directory)
                logger.info('Handling directory: "%s"' % path_directory)
                #self.paths_directory_input.extend(paths_directory_sub)
            else:
                logger.error('Error path: the directory "%s" does not exists!' % path_directory)


    def fill_files(self):
        for path_directory in self.paths_directory_input:
            paths_files = [f for f in os.listdir(path_directory) if os.path.isfile(os.path.join(path_directory, f))]
            paths_files_valid = [os.path.join(path_directory, f) for f in paths_files if self.rule_set.check_permission('RuleFilePermission', {'name_file': f})]
            self.paths_file_input.extend(paths_files_valid)

        # Logs
        for path in self.paths_file_input:
            logger.info('Handling file: "%s"' % path)


    def fill_gw_headers(self):
        for gw_file in self.gw_files.values():
            for header in self.rule_set.get_headers(gw_file.name_file):
                gw_file.headers.append(header)
                self.headers.append(header)
                logger.debug('Adding header "%s" to file "%s"', header, gw_file.name_file)
        self.headers = list(set(self.headers))


    def fill_ft_prototypes(self):
        for path_file in self.paths_file_input:
            file = open(path_file, 'r')
            prototypes = [self.tokenizer.string_to_prototype(s) for s in self.tokenizer.strings_prototype(file) if self.tokenizer.string_to_prototype(s)]
            self.ft_prototypes.extend(prototypes)
            file.close()


    def __parameter_ft_to_gw(self, ft_parameter):
        gw_parameter = None
        if ft_parameter:
            gw_parameter = GatewayParameter() 
            gw_parameter.name_type = ft_parameter.name_type
            gw_parameter.name_parameter = ft_parameter.name_parameter
            gw_parameter.position_source = ft_parameter.position
            gw_parameter.position_destination = ft_parameter.position
            gw_parameter.nb_pointers = self.tokenizer.count_pointers_in_string(gw_parameter.name_type)
        return gw_parameter


    def convert_ft_prototypes_to_gw_prototypes(self):
        for ft_prototype in self.ft_prototypes: 
            gw_prototype = GatewayPrototype()
            gw_prototype.name_function = ft_prototype.name_function
            gw_prototype.name_class = ft_prototype.name_class
            gw_prototype.comments = ft_prototype.comments

            gw_parameter_return = self.__parameter_ft_to_gw(ft_prototype.parameter_return) 
            if gw_parameter_return:
                gw_prototype.add_parameter_return(gw_parameter_return, position_return=1, position_source=-1)
                #gw_prototype.add_parameter_return(gw_prototype)

            for ft_parameter in ft_prototype.parameters:
                gw_parameter = self.__parameter_ft_to_gw(ft_parameter)
                gw_prototype.add_parameter_source(gw_parameter)
            self.gw_prototypes.append(gw_prototype)


    def lower_names_parameter_gw_prototypes(self):
        pattern = re.compile('([A-Z][A-Z][a-z])|([a-z][A-Z])')
        for gw_prototype in self.gw_prototypes:
            for gw_parameter in gw_prototype.parameters:
                if self.rule_set.is_lower_case_activated('parameters'):
                    name_parameter_lower = pattern.sub(lambda m: m.group()[:1] + '_' + m.group()[1:], gw_parameter.name_parameter)
                    gw_parameter.name_parameter_lower = name_parameter_lower.lower()
                else:
                    gw_parameter.name_parameter_lower = gw_parameter.name_parameter


    def replace_names_parameter_gw_prototypes(self):
        for gw_prototype in self.gw_prototypes:
            for gw_parameter in gw_prototype.parameters:
                name_parameter_new = gw_parameter.name_parameter_lower
                for source, destination in self.rule_set.get_replacements(name_parameter_new, 'parameter'):
                    name_parameter_new = re.sub(source, destination, name_parameter_new)
                gw_parameter.name_parameter_new = name_parameter_new


    def lower_names_function_gw_prototypes(self):
        pattern = re.compile('([A-Z][A-Z][a-z])|([a-z][A-Z])')
        for gw_prototype in self.gw_prototypes:
            if self.rule_set.is_lower_case_activated('functions'):
                name_function_lower = pattern.sub(lambda m: m.group()[:1] + '_' + m.group()[1:], gw_prototype.name_function)
                gw_prototype.name_function_lower = name_function_lower.lower()
            else:
                gw_prototype.name_function_lower = gw_prototype.name_function


    def replace_names_function_gw_prototypes(self):
        for gw_prototype in self.gw_prototypes:
            name_function_new = gw_prototype.name_function_lower
            for source, destination in self.rule_set.get_replacements(name_function_new, 'function'):
               name_function_new = re.sub(source, destination, name_function_new)
            gw_prototype.name_function_new = name_function_new


    def fill_names_gateway_gw_prototypes(self):
        for gw_prototype in self.gw_prototypes:
            gw_prototype.name_gateway = 'sci_' + gw_prototype.name_function_new


    def replace_names_function_in_comments(self):
        for gw_prototype_outer in self.gw_prototypes:
            if gw_prototype_outer.name_function != gw_prototype_outer.name_function_new:
                for gw_prototype_inner in self.gw_prototypes:
                    gw_prototype_inner.comments = gw_prototype_inner.comments.replace(gw_prototype_outer.name_function, gw_prototype_outer.name_function_new)


    def filter_gw_parameters(self):
        for gw_prototype in self.gw_prototypes:
            for gw_parameter in gw_prototype.get_parameters():
                checks = {'name_function': gw_prototype.name_function, \
                          'name_parameter': [gw_parameter.name_parameter, gw_parameter.name_type]}
                state_check = self.rule_set.check_permission('RuleParameterPermission', checks) 
                if state_check == False:
                    gw_parameter.position_destination = None
                    gw_parameter.position_return = None
                    logger.info('Excluding parameter: %s in %s()' % (gw_parameter.name_parameter, gw_prototype.name_function))


    def adjust_nb_parameters_destination(self):
        for gw_prototype in self.gw_prototypes:
            nb_parameters_destination = 0
            for gw_parameter in gw_prototype.get_parameters():
                if gw_parameter.position_destination != None:
                    nb_parameters_destination += 1
            gw_prototype.nb_parameters_destination = nb_parameters_destination


    def filter_ft_prototypes(self):
        new_ft_prototypes = []
        for ft_prototype in self.ft_prototypes:
            check = {'name_function': ft_prototype.name_function}
            state_check = self.rule_set.check_permission('RuleFunctionPermission', check)
            if state_check == True:
                new_ft_prototypes.append(ft_prototype)
        self.ft_prototypes = new_ft_prototypes
            # TODO check the other parameters
            # TODO how to handle the condition of the other parameters => influences the way we decide to check the rules, exclude then include?


    def rename_parameters(self):
        for gw_prototype in self.gw_prototypes:
            for gw_parameter in gw_prototype.parameters:
                gw_parameter.name_type_original = gw_parameter.name_type
                alias = self.rule_set.get_alias(gw_parameter.name_type)
                if alias != None:
                    # TODO check that this adding of * is valid
                    alias += '*' * gw_parameter.nb_pointers
                    logger.info('Aliasing type: from "%s" to "%s" for parameter %s in %s()' % (gw_parameter.name_type, alias, gw_parameter.name_parameter, gw_prototype.name_function))
                    gw_parameter.name_type = alias


    def add_gw_parameters(self):
        for gw_prototype in self.gw_prototypes:
            #state_name_function = self.rule_set.check_permission(gw_prototype.get_name_function(), 'name_function', 'RuleParameterAdd')
            # TODO replace none by the appropriate variable. There is maybe a confusion between class and file here, so check on it.
            for string_parameter in self.rule_set.get_parameters_added(gw_prototype.name_function, gw_prototype.get_strings_parameters(), None):
                ft_parameter = self.tokenizer.string_to_parameter(string_parameter)
                gw_parameter = self.__parameter_ft_to_gw(ft_parameter)
                # TODO add the possibility to add the parameter at the head
                # gw_parameter.set_position_destination(nb_parameters)
                gw_prototype.add_parameter_destination(gw_parameter)


    def shift_gw_parameters_to_return_list(self):
        for gw_prototype in self.gw_prototypes:
            #for gw_parameter in gw_prototype.get_parameters_source_and_destination():
            for gw_parameter in gw_prototype.get_parameters():
                checks = {'name_function': gw_prototype.name_function, \
                          'name_parameter': [gw_parameter.name_parameter, gw_parameter.name_type]}
                state_check, rule_return = self.rule_set.check_field('RuleParameterReturn', checks)
                if state_check == True and rule.check_regex_seq(rule_return.name_parameter_present, gw_prototype.get_strings_parameters()):
                    gw_parameter.position_return = GatewayParameter.POSITION_END


    def __set_parameter_dimension(self, gw_prototype, gw_parameter_matrix, gw_parameter):
        gw_parameter_dimension = gw_parameter.to_parameter_dimension()
        gw_parameter_dimension.parameter_matrix = gw_parameter_matrix
        gw_prototype.overwrite_parameter(gw_parameter, gw_parameter_dimension)
         

    def __set_parameter_matrix(self, rule_matrix, gw_prototype, gw_parameter):
        # Transform the parameter to a matrix parameter
        gw_parameter_matrix = gw_parameter.to_parameter_matrix()
        gw_parameter_matrix.parameter_dimension_row = gw_prototype.get_parameter_by_name(rule_matrix.name_dim_row)
        gw_parameter_matrix.parameter_dimension_column = gw_prototype.get_parameter_by_name(rule_matrix.name_dim_col)
        gw_prototype.overwrite_parameter(gw_parameter, gw_parameter_matrix)

        #print 'set dimension:', gw_parameter.name_parameter
        # Need to transform the dimension parameter too
        if gw_parameter_matrix.parameter_dimension_row:
            self.__set_parameter_dimension(gw_prototype, gw_parameter_matrix, gw_parameter_matrix.parameter_dimension_row)
            #print 'row:', gw_parameter_matrix.parameter_dimension_row
        if gw_parameter_matrix.parameter_dimension_column:
            self.__set_parameter_dimension(gw_prototype, gw_parameter_matrix, gw_parameter_matrix.parameter_dimension_column)
            #print 'col:', gw_parameter_matrix.parameter_dimension_column
        

    def set_parameters_matrix(self):
        for gw_prototype in self.gw_prototypes:
            for gw_parameter in gw_prototype.get_parameters_source_and_destination():
            # TODO replace none by the appropriate variable. There is maybe a confusion between class and file here, so check on it.
                found, rule_matrix = self.rule_set.get_rule_parameters_matrix(gw_parameter.name_parameter, None, gw_prototype.get_strings_parameters(), None)
                if found:
                    self.__set_parameter_matrix(rule_matrix, gw_prototype, gw_parameter)


    def fill_and_check_names_type(self):
        for gw_prototype in self.gw_prototypes:
            for gw_parameter in gw_prototype.get_parameters():
                name_type = self.tokenizer.get_type_in_string(gw_parameter.name_type)
                # TODO this test is just a patch, find the problem and fix it!
                if name_type:
                    names_type = self.names_type_valid if self.tokenizer.is_name_type_valid(name_type) else self.names_type_user
                    names_type.append(name_type)
                else:
                    logger.error('Type unknown for parameter "%s" in %s(): the function has been ignored.' % (gw_parameter.name_parameter, gw_prototype.name_function))
                    gw_prototype.error_type = True

        self.names_type_valid = list(set(self.names_type_valid))
        self.names_type_user = list(set(self.names_type_user))

        logger.debug('Valid types: %s' % self.names_type_valid)
        logger.debug('User types: %s' % self.names_type_user)


    def fill_names_type_non_double(self):
        self.names_type_non_double = [name_type for name_type in self.names_type_valid if self.tokenizer.is_conversion_to_double_needed(name_type)]
        logger.debug('Non double types: %s' % self.names_type_non_double)
        

    def set_hooks(self):
        for gw_prototype in self.gw_prototypes:
            for location, line in self.rule_set.get_hooks(gw_prototype.name_function, gw_prototype.get_strings_parameters(), None):
                if location == 'start':
                    gw_prototype.add_hook_start(line)
                else: # location == 'end'
                    gw_prototype.add_hook_end(line)


    def fill_gw_files(self):
        # Create all the files specified in the rules
        for rule_file in self.rule_set.get_rules_by_type('RuleFile'):
            self.gw_files[rule_file.name_file] = GatewayFile(rule_file.name_file)
        self.gw_files[self.name_file_default] = GatewayFile(self.name_file_default)

        #for gw_prototype in self.gw_prototypes:
        #    self.gw_files[self.name_file_default].gw_prototypes.append(gw_prototype)


    def filter_gw_files(self):
        for gw_prototype in self.gw_prototypes:
            found, rule_file = self.rule_set.get_rule_file(gw_prototype.name_function)
            if not gw_prototype.error_type:
                if found and rule.check_regex_seq(rule_file.name_parameter_present, gw_prototype.get_strings_parameters()):
                    self.gw_files[rule_file.name_file].gw_prototypes.append(gw_prototype)
                else:
                    # By default, all the prototypes are added to the default file
                    self.gw_files[self.name_file_default].gw_prototypes.append(gw_prototype)

        # Delete from the default files the prototypes that have just been moved
        # to other files
        #for index in reversed(indices):
        #    del self.gw_files[self.name_file_default].gw_prototypes[index]

    # TODO: code a common function for get_files_*
    
    def get_files_user_types(self, headers):
        if self.names_type_user:
            self.gw_writer.write_helper('user_types', self.names_type_user, 'Handing special type', ['#include "stack-c.h"', '#include "Scierror.h"', '#define POS_LABELS 1', '#define POS_STRUCT_POINTER 2'] + headers)
            header_user_types = '#include "' + self.gw_writer.name_file_helper_header_output % 'user_types' + '"'
            body_user_types = self.gw_writer.name_file_helper_body_output % 'user_types'
        else:
            header_user_types = None
            body_user_types = None

        return(header_user_types, body_user_types)


    def get_files_type_converters(self, headers):
        if self.names_type_non_double:
            self.gw_writer.write_helper('type_converters', self.names_type_non_double, 'Converting type', ['#include <stdlib.h>', '#include "stack-c.h"', '#include "Scierror.h"', '#include "localization.h"', '#include "sciprint.h"'] + headers)
            header_type_converters = '#include "' + self.gw_writer.name_file_helper_header_output % 'type_converters' + '"'
            body_type_converters = self.gw_writer.name_file_helper_body_output % 'type_converters'
        else:
            header_type_converters = None
            body_type_converters = None

        return(header_type_converters, body_type_converters)

    # TODO: put in a parameter the name of the directory where the doc files
    # have to be written
    def write_gw(self, path_directory_output):
        self.gw_writer.init_output_directory(path_directory_output)
        (header_user_types, body_user_types) = self.get_files_user_types(self.headers)
        (header_type_converters, body_type_converters) = self.get_files_type_converters(self.headers)
        self.gw_writer.write_gw_files(self.gw_files, [header_user_types, header_type_converters])
        self.gw_writer.write_documentation_prototypes(self.gw_prototypes, 'doc', self.authors)
        self.gw_writer.write_builder(self.gw_files, self.gw_prototypes, self.name_project, self.language, [body_user_types, body_type_converters], self.compiler_libraries, self.compiler_links, self.compiler_includes)


    def generate(self, path_file_config, path_directory_output):

        # Handle rules
        self.load_rule_file(path_file_config)
        self.filter_rules()

        # Handle configuration parameters
        self.fill_configuration_parameters()

        # Handle the directories and files
        self.fill_directories()
        self.fill_files()

        # Handle the prototypes
        self.fill_ft_prototypes()
        self.filter_ft_prototypes()
        self.convert_ft_prototypes_to_gw_prototypes()
        self.lower_names_function_gw_prototypes()
        self.replace_names_function_gw_prototypes()
        self.fill_names_gateway_gw_prototypes()
        self.replace_names_function_in_comments()

        # Handle the parameters
        self.add_gw_parameters()
        self.filter_gw_parameters()
        self.adjust_nb_parameters_destination()
        self.shift_gw_parameters_to_return_list()
        self.set_parameters_matrix()
        self.rename_parameters()
        self.fill_and_check_names_type()
        self.fill_names_type_non_double()
        self.lower_names_parameter_gw_prototypes()
        self.replace_names_parameter_gw_prototypes()

        # Handle the files
        self.fill_gw_files()
        self.filter_gw_files()
        self.fill_gw_headers()

        # Handle the hooks
        self.set_hooks()

        # Write the gateway files
        self.write_gw(path_directory_output)
            

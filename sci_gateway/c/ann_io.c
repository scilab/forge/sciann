#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


krui_err krui_saveNet(char*, char*);
krui_err krui_loadNet(char*, char**);
krui_err krui_saveResultParam(char*, bool, int, int, bool, bool, float*, int);


int sci_ann_save_net(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_filename;
    char *var_netname;

    /*** Dimension parameters ***/
    int res_network;
    int row_filename = 1, col_filename = 1;
    int row_netname = 1, col_netname = 1;

    /*** Address parameters ***/
    int *ptr_filename = NULL;
    int cnt_filename;
    int *lengths_filename = NULL;
    char **strings_filename = NULL;
    int *ptr_netname = NULL;
    int cnt_netname;
    int *lengths_netname = NULL;
    char **strings_netname = NULL;

    /*** Source parameters ***/
    int sci;
    char* filename;
    char* netname;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_filename);
    getVarAddressFromPosition(3, &ptr_netname);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_filename, &row_filename, &col_filename);
    getVarDimension(ptr_netname, &row_netname, &col_netname);

    /*** Prerequisites destination ***/
    lengths_filename = (int*) malloc(row_filename * col_filename * sizeof(int));
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, NULL);
    strings_filename = (char**) malloc(row_filename * col_filename * sizeof(char*));
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) strings_filename[cnt_filename] = (char*) malloc((lengths_filename[cnt_filename] + 1) * sizeof(char));
    lengths_netname = (int*) malloc(row_netname * col_netname * sizeof(int));
    getMatrixOfString(ptr_netname, &row_netname, &col_netname, lengths_netname, NULL);
    strings_netname = (char**) malloc(row_netname * col_netname * sizeof(char*));
    for(cnt_netname = 0; cnt_netname < row_netname * col_netname ; ++cnt_netname) strings_netname[cnt_netname] = (char*) malloc((lengths_netname[cnt_netname] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, strings_filename);
    getMatrixOfString(ptr_netname, &row_netname, &col_netname, lengths_netname, strings_netname);

    /*** Parameter copy ***/
    filename = strings_filename[0];
    netname = strings_netname[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_saveNet(filename, netname);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) free(strings_filename[cnt_filename]);
    free(strings_filename);
    free(lengths_filename);
    for(cnt_netname = 0; cnt_netname < row_netname * col_netname ; ++cnt_netname) free(strings_netname[cnt_netname]);
    free(strings_netname);
    free(lengths_netname);


    return 0;
}


int sci_ann_load_net(char *fname)
{
    /*** Destination parameters ***/
    char *var_filename;
    memNet *var_network_ret;
    char *var_netname_ret;

    /*** Dimension parameters ***/
    int row_filename = 1, col_filename = 1;
    int row_netname = 1, col_netname = 1;
    int res_network_ret;
    int row_netname_ret = 1, col_netname_ret = 1;

    /*** Address parameters ***/
    int *ptr_filename = NULL;
    int cnt_filename;
    int *lengths_filename = NULL;
    char **strings_filename = NULL;
    int *ptr_netname_ret = NULL;
    int cnt_netname_ret;
    int *lengths_netname_ret = NULL;
    char **strings_netname_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* filename;
    char** netname;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    getVarAddressFromPosition(1, &ptr_filename);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_filename, &row_filename, &col_filename);

    /*** Prerequisites destination ***/
    lengths_filename = (int*) malloc(row_filename * col_filename * sizeof(int));
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, NULL);
    strings_filename = (char**) malloc(row_filename * col_filename * sizeof(char*));
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) strings_filename[cnt_filename] = (char*) malloc((lengths_filename[cnt_filename] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, strings_filename);

    /*** Memory allocation ***/
    netname = (char**) malloc(sizeof(char*));

    /*** Parameter copy ***/
    filename = strings_filename[0];


    // *** Function call
    sci = krui_loadNet(filename, netname);

    /*** Hook functions end ***/
    krm_getNet(var_network_ret);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** String check ***/
    for(cnt_netname_ret = 0; cnt_netname_ret < row_netname_ret * col_netname_ret ; ++cnt_netname_ret) if(netname[cnt_netname_ret] == NULL) { Scierror(999, "netname is NULL"); return -1;}

    /*** Create return parameters ***/
    res_network_ret = ctos_memNet(2, var_network_ret);
    strings_netname_ret = (char**) malloc(row_netname_ret * col_netname_ret * sizeof(char*));
    for(cnt_netname_ret = 0; cnt_netname_ret < row_netname_ret * col_netname_ret ; ++cnt_netname_ret) strings_netname_ret[cnt_netname_ret] = netname[cnt_netname_ret];
    createMatrixOfString(3, row_netname_ret, col_netname_ret, strings_netname_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;

    /*** Postrequisites destination ***/
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) free(strings_filename[cnt_filename]);
    free(strings_filename);
    free(lengths_filename);

    /*** Postrequisites return ***/
    free(strings_netname_ret);

    /*** Memory free ***/
    free(netname);


    return 0;
}


int sci_ann_save_result_param(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_filename;
    int *var_create;
    int *var_startpattern;
    int *var_endpattern;
    int *var_includeinput;
    int *var_includeoutput;
    float *var_update_param_array;

    /*** Dimension parameters ***/
    int res_network;
    int row_filename = 1, col_filename = 1;
    int row_create = 1, col_create = 1;
    int row_startpattern = 1, col_startpattern = 1;
    int row_endpattern = 1, col_endpattern = 1;
    int row_includeinput = 1, col_includeinput = 1;
    int row_includeoutput = 1, col_includeoutput = 1;
    int row_update_param_array = 1, col_update_param_array = 1;
    int row_no_of_update_param = 1, col_no_of_update_param = 1;

    /*** Address parameters ***/
    int *ptr_filename = NULL;
    int cnt_filename;
    int *lengths_filename = NULL;
    char **strings_filename = NULL;
    int *ptr_create = NULL;
    int *ptr_startpattern = NULL;
    int *ptr_endpattern = NULL;
    int *ptr_includeinput = NULL;
    int *ptr_includeoutput = NULL;
    int *ptr_update_param_array = NULL;

    /*** Source parameters ***/
    int sci;
    char* filename;
    int create;
    int startpattern;
    int endpattern;
    int includeinput;
    int includeoutput;
    float* update_param_array;
    int no_of_update_param;

    /*** Check for number of parameters ***/
    CheckRhs(8, 8);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_filename);
    getVarAddressFromPosition(3, &ptr_create);
    getVarAddressFromPosition(4, &ptr_startpattern);
    getVarAddressFromPosition(5, &ptr_endpattern);
    getVarAddressFromPosition(6, &ptr_includeinput);
    getVarAddressFromPosition(7, &ptr_includeoutput);
    getVarAddressFromPosition(8, &ptr_update_param_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_filename, &row_filename, &col_filename);
    getVarDimension(ptr_create, &row_create, &col_create);
    getVarDimension(ptr_startpattern, &row_startpattern, &col_startpattern);
    getVarDimension(ptr_endpattern, &row_endpattern, &col_endpattern);
    getVarDimension(ptr_includeinput, &row_includeinput, &col_includeinput);
    getVarDimension(ptr_includeoutput, &row_includeoutput, &col_includeoutput);
    getVarDimension(ptr_update_param_array, &row_update_param_array, &col_update_param_array);

    /*** Prerequisites destination ***/
    lengths_filename = (int*) malloc(row_filename * col_filename * sizeof(int));
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, NULL);
    strings_filename = (char**) malloc(row_filename * col_filename * sizeof(char*));
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) strings_filename[cnt_filename] = (char*) malloc((lengths_filename[cnt_filename] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_filename, &row_filename, &col_filename, lengths_filename, strings_filename);
    getMatrixFromDoubleTo_int(ptr_create, &row_create, &col_create, &var_create);
    getMatrixFromDoubleTo_int(ptr_startpattern, &row_startpattern, &col_startpattern, &var_startpattern);
    getMatrixFromDoubleTo_int(ptr_endpattern, &row_endpattern, &col_endpattern, &var_endpattern);
    getMatrixFromDoubleTo_int(ptr_includeinput, &row_includeinput, &col_includeinput, &var_includeinput);
    getMatrixFromDoubleTo_int(ptr_includeoutput, &row_includeoutput, &col_includeoutput, &var_includeoutput);
    getMatrixFromDoubleTo_float(ptr_update_param_array, &row_update_param_array, &col_update_param_array, &var_update_param_array);

    /*** Parameter copy ***/
    filename = strings_filename[0];
    create = *var_create;
    startpattern = *var_startpattern;
    endpattern = *var_endpattern;
    includeinput = *var_includeinput;
    includeoutput = *var_includeoutput;
    update_param_array = var_update_param_array;
    no_of_update_param = col_update_param_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_saveResultParam(filename, create, startpattern, endpattern, includeinput, includeoutput, update_param_array, no_of_update_param);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_filename = 0; cnt_filename < row_filename * col_filename ; ++cnt_filename) free(strings_filename[cnt_filename]);
    free(strings_filename);
    free(lengths_filename);


    return 0;
}



#include <stdlib.h>
#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "glob_typ.h"
#include "enzo_mem_typ.h"
#include "kr_typ.h"
#include "config.h"
#include "kr_mem.h"
#include "kr_ui.h"

int getMatrixFromDoubleTo_int(int* _piAddress, int* _piRows, int* _piCols, int** _pdblReal);
int createMatrixOfDoubleFrom_int(int _iVar, int _piRows, int _piCols, int* _pdblReal);


int getMatrixFromDoubleTo_float(int* _piAddress, int* _piRows, int* _piCols, float** _pdblReal);
int createMatrixOfDoubleFrom_float(int _iVar, int _piRows, int _piCols, float* _pdblReal);


int getMatrixFromDoubleTo_char(int* _piAddress, int* _piRows, int* _piCols, char** _pdblReal);
int createMatrixOfDoubleFrom_char(int _iVar, int _piRows, int _piCols, char* _pdblReal);


int getMatrixFromDoubleTo_unsigned_short(int* _piAddress, int* _piRows, int* _piCols, unsigned short** _pdblReal);
int createMatrixOfDoubleFrom_unsigned_short(int _iVar, int _piRows, int _piCols, unsigned short* _pdblReal);


int getMatrixFromDoubleTo_long_int(int* _piAddress, int* _piRows, int* _piCols, long int** _pdblReal);
int createMatrixOfDoubleFrom_long_int(int _iVar, int _piRows, int _piCols, long int* _pdblReal);


int getMatrixFromDoubleTo_unsigned_int(int* _piAddress, int* _piRows, int* _piCols, unsigned int** _pdblReal);
int createMatrixOfDoubleFrom_unsigned_int(int _iVar, int _piRows, int _piCols, unsigned int* _pdblReal);

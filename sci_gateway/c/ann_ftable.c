#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


int krui_getNoOfFunctions();
void krui_getFuncInfo(int, char**, int*);
bool krui_isFunction(char*, int);
bool krui_getFuncParamInfo(char*, int, int*, int*);


int sci_ann_get_no_of_functions(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNoOfFunctions();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_func_info(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_func_id;
    char *var_func_name_ret;
    int *var_func_type_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_func_id = 1, col_func_id = 1;
    int row_func_name = 1, col_func_name = 1;
    int row_func_type = 1, col_func_type = 1;
    int row_func_name_ret = 1, col_func_name_ret = 1;
    int row_func_type_ret = 1, col_func_type_ret = 1;

    /*** Address parameters ***/
    int *ptr_func_id = NULL;
    int *ptr_func_name_ret = NULL;
    int cnt_func_name_ret;
    int *lengths_func_name_ret = NULL;
    char **strings_func_name_ret = NULL;
    int *ptr_func_type_ret = NULL;

    /*** Source parameters ***/
    int func_id;
    char** func_name;
    int* func_type;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(2, 2);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_func_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_func_id, &row_func_id, &col_func_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_func_id, &row_func_id, &col_func_id, &var_func_id);

    /*** Memory allocation ***/
    func_name = (char**) malloc(sizeof(char*));
    func_type = (int*) malloc(row_func_type * col_func_type * sizeof(int));

    /*** Parameter copy ***/
    func_id = *var_func_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_getFuncInfo(func_id, func_name, func_type);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    for(cnt_func_name_ret = 0; cnt_func_name_ret < row_func_name_ret * col_func_name_ret ; ++cnt_func_name_ret) if(func_name[cnt_func_name_ret] == NULL) { Scierror(999, "func_name is NULL"); return -1;}

    /*** Create return parameters ***/
    strings_func_name_ret = (char**) malloc(row_func_name_ret * col_func_name_ret * sizeof(char*));
    for(cnt_func_name_ret = 0; cnt_func_name_ret < row_func_name_ret * col_func_name_ret ; ++cnt_func_name_ret) strings_func_name_ret[cnt_func_name_ret] = func_name[cnt_func_name_ret];
    createMatrixOfString(3, row_func_name_ret, col_func_name_ret, strings_func_name_ret);
    createMatrixOfDoubleFrom_int(4, row_func_type_ret, col_func_type_ret, func_type);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;
    LhsVar(2) = 4;

    /*** Postrequisites return ***/
    free(strings_func_name_ret);

    /*** Memory free ***/
    free(func_name);
    free(func_type);


    return 0;
}


int sci_ann_is_function(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_func_name;
    int *var_func_type;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_func_name = 1, col_func_name = 1;
    int row_func_type = 1, col_func_type = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_func_name = NULL;
    int cnt_func_name;
    int *lengths_func_name = NULL;
    char **strings_func_name = NULL;
    int *ptr_func_type = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* func_name;
    int func_type;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_func_name);
    getVarAddressFromPosition(3, &ptr_func_type);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_func_name, &row_func_name, &col_func_name);
    getVarDimension(ptr_func_type, &row_func_type, &col_func_type);

    /*** Prerequisites destination ***/
    lengths_func_name = (int*) malloc(row_func_name * col_func_name * sizeof(int));
    getMatrixOfString(ptr_func_name, &row_func_name, &col_func_name, lengths_func_name, NULL);
    strings_func_name = (char**) malloc(row_func_name * col_func_name * sizeof(char*));
    for(cnt_func_name = 0; cnt_func_name < row_func_name * col_func_name ; ++cnt_func_name) strings_func_name[cnt_func_name] = (char*) malloc((lengths_func_name[cnt_func_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_func_name, &row_func_name, &col_func_name, lengths_func_name, strings_func_name);
    getMatrixFromDoubleTo_int(ptr_func_type, &row_func_type, &col_func_type, &var_func_type);

    /*** Parameter copy ***/
    func_name = strings_func_name[0];
    func_type = *var_func_type;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_isFunction(func_name, func_type);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(4, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;

    /*** Postrequisites destination ***/
    for(cnt_func_name = 0; cnt_func_name < row_func_name * col_func_name ; ++cnt_func_name) free(strings_func_name[cnt_func_name]);
    free(strings_func_name);
    free(lengths_func_name);


    return 0;
}


int sci_ann_get_func_param_info(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_func_name;
    int *var_func_type;
    int *var_sci_ret;
    int *var_no_of_input_params_ret;
    int *var_no_of_output_params_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_func_name = 1, col_func_name = 1;
    int row_func_type = 1, col_func_type = 1;
    int row_no_of_input_params = 1, col_no_of_input_params = 1;
    int row_no_of_output_params = 1, col_no_of_output_params = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_no_of_input_params_ret = 1, col_no_of_input_params_ret = 1;
    int row_no_of_output_params_ret = 1, col_no_of_output_params_ret = 1;

    /*** Address parameters ***/
    int *ptr_func_name = NULL;
    int cnt_func_name;
    int *lengths_func_name = NULL;
    char **strings_func_name = NULL;
    int *ptr_func_type = NULL;
    int *ptr_sci_ret = NULL;
    int *ptr_no_of_input_params_ret = NULL;
    int *ptr_no_of_output_params_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* func_name;
    int func_type;
    int* no_of_input_params;
    int* no_of_output_params;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(3, 3);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_func_name);
    getVarAddressFromPosition(3, &ptr_func_type);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_func_name, &row_func_name, &col_func_name);
    getVarDimension(ptr_func_type, &row_func_type, &col_func_type);

    /*** Prerequisites destination ***/
    lengths_func_name = (int*) malloc(row_func_name * col_func_name * sizeof(int));
    getMatrixOfString(ptr_func_name, &row_func_name, &col_func_name, lengths_func_name, NULL);
    strings_func_name = (char**) malloc(row_func_name * col_func_name * sizeof(char*));
    for(cnt_func_name = 0; cnt_func_name < row_func_name * col_func_name ; ++cnt_func_name) strings_func_name[cnt_func_name] = (char*) malloc((lengths_func_name[cnt_func_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_func_name, &row_func_name, &col_func_name, lengths_func_name, strings_func_name);
    getMatrixFromDoubleTo_int(ptr_func_type, &row_func_type, &col_func_type, &var_func_type);

    /*** Memory allocation ***/
    no_of_input_params = (int*) malloc(row_no_of_input_params * col_no_of_input_params * sizeof(int));
    no_of_output_params = (int*) malloc(row_no_of_output_params * col_no_of_output_params * sizeof(int));

    /*** Parameter copy ***/
    func_name = strings_func_name[0];
    func_type = *var_func_type;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFuncParamInfo(func_name, func_type, no_of_input_params, no_of_output_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(4, row_sci_ret, col_sci_ret, &sci);
    createMatrixOfDoubleFrom_int(5, row_no_of_input_params_ret, col_no_of_input_params_ret, no_of_input_params);
    createMatrixOfDoubleFrom_int(6, row_no_of_output_params_ret, col_no_of_output_params_ret, no_of_output_params);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;
    LhsVar(2) = 5;
    LhsVar(3) = 6;

    /*** Postrequisites destination ***/
    for(cnt_func_name = 0; cnt_func_name < row_func_name * col_func_name ; ++cnt_func_name) free(strings_func_name[cnt_func_name]);
    free(strings_func_name);
    free(lengths_func_name);

    /*** Memory free ***/
    free(no_of_input_params);
    free(no_of_output_params);


    return 0;
}



#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


krui_err krui_jogCorrWeights(FlintTypeParam, FlintTypeParam, FlintTypeParam);
float krui_getVariance();
krui_err krui_setRemapFunc(char*, float*);
krui_err krui_setClassDistribution(unsigned int*);
krui_err krui_setClassInfo(char*);
krui_err krui_useClassDistribution(bool);
char* krui_getVersion();
void krui_getNetInfo(int*, int*, int*, int*);
void krui_getUnitDefaults(FlintType*, FlintType*, int*, int*, int*, char**, char**);
krui_err krui_setUnitDefaults(FlintTypeParam, FlintTypeParam, int, int, int, char*, char*);
void krui_resetNet();
void krui_setSeedNo(long int);
int krui_getNoOfInputUnits();
int krui_getNoOfOutputUnits();
int krui_getNoOfSpecialInputUnits();
int krui_getNoOfSpecialOutputUnits();


int sci_ann_jog_corr_weights(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    double *var_minus;
    double *var_plus;
    double *var_mincorr;

    /*** Dimension parameters ***/
    int res_network;
    int row_minus = 1, col_minus = 1;
    int row_plus = 1, col_plus = 1;
    int row_mincorr = 1, col_mincorr = 1;

    /*** Address parameters ***/
    int *ptr_minus = NULL;
    int *ptr_plus = NULL;
    int *ptr_mincorr = NULL;

    /*** Source parameters ***/
    int sci;
    double minus;
    double plus;
    double mincorr;

    /*** Check for number of parameters ***/
    CheckRhs(4, 4);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_minus);
    getVarAddressFromPosition(3, &ptr_plus);
    getVarAddressFromPosition(4, &ptr_mincorr);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_minus, &row_minus, &col_minus);
    getVarDimension(ptr_plus, &row_plus, &col_plus);
    getVarDimension(ptr_mincorr, &row_mincorr, &col_mincorr);

    /*** Getting parameters from stack ***/
    getMatrixOfDouble(ptr_minus, &row_minus, &col_minus, &var_minus);
    getMatrixOfDouble(ptr_plus, &row_plus, &col_plus, &var_plus);
    getMatrixOfDouble(ptr_mincorr, &row_mincorr, &col_mincorr, &var_mincorr);

    /*** Parameter copy ***/
    minus = *var_minus;
    plus = *var_plus;
    mincorr = *var_mincorr;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_jogCorrWeights(minus, plus, mincorr);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_variance(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    float *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    float sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getVariance();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_float(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_set_remap_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_name;
    float *var_params;

    /*** Dimension parameters ***/
    int res_network;
    int row_name = 1, col_name = 1;
    int row_params = 1, col_params = 1;

    /*** Address parameters ***/
    int *ptr_name = NULL;
    int cnt_name;
    int *lengths_name = NULL;
    char **strings_name = NULL;
    int *ptr_params = NULL;

    /*** Source parameters ***/
    int sci;
    char* name;
    float* params;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_name);
    getVarAddressFromPosition(3, &ptr_params);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_name, &row_name, &col_name);
    getVarDimension(ptr_params, &row_params, &col_params);

    /*** Prerequisites destination ***/
    lengths_name = (int*) malloc(row_name * col_name * sizeof(int));
    getMatrixOfString(ptr_name, &row_name, &col_name, lengths_name, NULL);
    strings_name = (char**) malloc(row_name * col_name * sizeof(char*));
    for(cnt_name = 0; cnt_name < row_name * col_name ; ++cnt_name) strings_name[cnt_name] = (char*) malloc((lengths_name[cnt_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_name, &row_name, &col_name, lengths_name, strings_name);
    getMatrixFromDoubleTo_float(ptr_params, &row_params, &col_params, &var_params);

    /*** Parameter copy ***/
    name = strings_name[0];
    params = var_params;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setRemapFunc(name, params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_name = 0; cnt_name < row_name * col_name ; ++cnt_name) free(strings_name[cnt_name]);
    free(strings_name);
    free(lengths_name);


    return 0;
}


int sci_ann_set_class_distribution(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    unsigned int *var_class_dist;

    /*** Dimension parameters ***/
    int res_network;
    int row_class_dist = 1, col_class_dist = 1;

    /*** Address parameters ***/
    int *ptr_class_dist = NULL;

    /*** Source parameters ***/
    int sci;
    unsigned int* class_dist;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_class_dist);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_class_dist, &row_class_dist, &col_class_dist);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_unsigned_int(ptr_class_dist, &row_class_dist, &col_class_dist, &var_class_dist);

    /*** Parameter copy ***/
    class_dist = var_class_dist;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setClassDistribution(class_dist);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_set_class_info(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_name;

    /*** Dimension parameters ***/
    int res_network;
    int row_name = 1, col_name = 1;

    /*** Address parameters ***/
    int *ptr_name = NULL;
    int cnt_name;
    int *lengths_name = NULL;
    char **strings_name = NULL;

    /*** Source parameters ***/
    int sci;
    char* name;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_name);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_name, &row_name, &col_name);

    /*** Prerequisites destination ***/
    lengths_name = (int*) malloc(row_name * col_name * sizeof(int));
    getMatrixOfString(ptr_name, &row_name, &col_name, lengths_name, NULL);
    strings_name = (char**) malloc(row_name * col_name * sizeof(char*));
    for(cnt_name = 0; cnt_name < row_name * col_name ; ++cnt_name) strings_name[cnt_name] = (char*) malloc((lengths_name[cnt_name] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_name, &row_name, &col_name, lengths_name, strings_name);

    /*** Parameter copy ***/
    name = strings_name[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setClassInfo(name);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_name = 0; cnt_name < row_name * col_name ; ++cnt_name) free(strings_name[cnt_name]);
    free(strings_name);
    free(lengths_name);


    return 0;
}


int sci_ann_use_class_distribution(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_use_it;

    /*** Dimension parameters ***/
    int res_network;
    int row_use_it = 1, col_use_it = 1;

    /*** Address parameters ***/
    int *ptr_use_it = NULL;

    /*** Source parameters ***/
    int sci;
    int use_it;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_use_it);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_use_it, &row_use_it, &col_use_it);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_use_it, &row_use_it, &col_use_it, &var_use_it);

    /*** Parameter copy ***/
    use_it = *var_use_it;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_useClassDistribution(use_it);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_version(char *fname)
{
    /*** Destination parameters ***/
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(0, 1);
    CheckLhs(1, 1);


    // *** Function call
    sci = krui_getVersion();

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(1, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 1;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_get_net_info(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_no_of_sites_ret;
    int *var_no_of_links_ret;
    int *var_no_of_s_table_entries_ret;
    int *var_no_of_f_table_entries_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_no_of_sites = 1, col_no_of_sites = 1;
    int row_no_of_links = 1, col_no_of_links = 1;
    int row_no_of_s_table_entries = 1, col_no_of_s_table_entries = 1;
    int row_no_of_f_table_entries = 1, col_no_of_f_table_entries = 1;
    int row_no_of_sites_ret = 1, col_no_of_sites_ret = 1;
    int row_no_of_links_ret = 1, col_no_of_links_ret = 1;
    int row_no_of_s_table_entries_ret = 1, col_no_of_s_table_entries_ret = 1;
    int row_no_of_f_table_entries_ret = 1, col_no_of_f_table_entries_ret = 1;

    /*** Address parameters ***/
    int *ptr_no_of_sites_ret = NULL;
    int *ptr_no_of_links_ret = NULL;
    int *ptr_no_of_s_table_entries_ret = NULL;
    int *ptr_no_of_f_table_entries_ret = NULL;

    /*** Source parameters ***/
    int* no_of_sites;
    int* no_of_links;
    int* no_of_s_table_entries;
    int* no_of_f_table_entries;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(4, 4);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    no_of_sites = (int*) malloc(row_no_of_sites * col_no_of_sites * sizeof(int));
    no_of_links = (int*) malloc(row_no_of_links * col_no_of_links * sizeof(int));
    no_of_s_table_entries = (int*) malloc(row_no_of_s_table_entries * col_no_of_s_table_entries * sizeof(int));
    no_of_f_table_entries = (int*) malloc(row_no_of_f_table_entries * col_no_of_f_table_entries * sizeof(int));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_getNetInfo(no_of_sites, no_of_links, no_of_s_table_entries, no_of_f_table_entries);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_no_of_sites_ret, col_no_of_sites_ret, no_of_sites);
    createMatrixOfDoubleFrom_int(3, row_no_of_links_ret, col_no_of_links_ret, no_of_links);
    createMatrixOfDoubleFrom_int(4, row_no_of_s_table_entries_ret, col_no_of_s_table_entries_ret, no_of_s_table_entries);
    createMatrixOfDoubleFrom_int(5, row_no_of_f_table_entries_ret, col_no_of_f_table_entries_ret, no_of_f_table_entries);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;
    LhsVar(3) = 4;
    LhsVar(4) = 5;

    /*** Memory free ***/
    free(no_of_sites);
    free(no_of_links);
    free(no_of_s_table_entries);
    free(no_of_f_table_entries);


    return 0;
}


int sci_ann_get_unit_defaults(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    double *var_act_ret;
    double *var_bias_ret;
    int *var_io_type_ret;
    int *var_subnet_id_ret;
    int *var_layer_id_ret;
    char *var_act_func_ret;
    char *var_out_func_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_act = 1, col_act = 1;
    int row_bias = 1, col_bias = 1;
    int row_io_type = 1, col_io_type = 1;
    int row_subnet_id = 1, col_subnet_id = 1;
    int row_layer_id = 1, col_layer_id = 1;
    int row_act_func = 1, col_act_func = 1;
    int row_out_func = 1, col_out_func = 1;
    int row_act_ret = 1, col_act_ret = 1;
    int row_bias_ret = 1, col_bias_ret = 1;
    int row_io_type_ret = 1, col_io_type_ret = 1;
    int row_subnet_id_ret = 1, col_subnet_id_ret = 1;
    int row_layer_id_ret = 1, col_layer_id_ret = 1;
    int row_act_func_ret = 1, col_act_func_ret = 1;
    int row_out_func_ret = 1, col_out_func_ret = 1;

    /*** Address parameters ***/
    int *ptr_act_ret = NULL;
    int *ptr_bias_ret = NULL;
    int *ptr_io_type_ret = NULL;
    int *ptr_subnet_id_ret = NULL;
    int *ptr_layer_id_ret = NULL;
    int *ptr_act_func_ret = NULL;
    int cnt_act_func_ret;
    int *lengths_act_func_ret = NULL;
    char **strings_act_func_ret = NULL;
    int *ptr_out_func_ret = NULL;
    int cnt_out_func_ret;
    int *lengths_out_func_ret = NULL;
    char **strings_out_func_ret = NULL;

    /*** Source parameters ***/
    double* act;
    double* bias;
    int* io_type;
    int* subnet_id;
    int* layer_id;
    char** act_func;
    char** out_func;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(7, 7);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    act = (double*) malloc(row_act * col_act * sizeof(double));
    bias = (double*) malloc(row_bias * col_bias * sizeof(double));
    io_type = (int*) malloc(row_io_type * col_io_type * sizeof(int));
    subnet_id = (int*) malloc(row_subnet_id * col_subnet_id * sizeof(int));
    layer_id = (int*) malloc(row_layer_id * col_layer_id * sizeof(int));
    act_func = (char**) malloc(sizeof(char*));
    out_func = (char**) malloc(sizeof(char*));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_getUnitDefaults(act, bias, io_type, subnet_id, layer_id, act_func, out_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    for(cnt_act_func_ret = 0; cnt_act_func_ret < row_act_func_ret * col_act_func_ret ; ++cnt_act_func_ret) if(act_func[cnt_act_func_ret] == NULL) { Scierror(999, "act_func is NULL"); return -1;}
    for(cnt_out_func_ret = 0; cnt_out_func_ret < row_out_func_ret * col_out_func_ret ; ++cnt_out_func_ret) if(out_func[cnt_out_func_ret] == NULL) { Scierror(999, "out_func is NULL"); return -1;}

    /*** Create return parameters ***/
    createMatrixOfDouble(2, row_act_ret, col_act_ret, act);
    createMatrixOfDouble(3, row_bias_ret, col_bias_ret, bias);
    createMatrixOfDoubleFrom_int(4, row_io_type_ret, col_io_type_ret, io_type);
    createMatrixOfDoubleFrom_int(5, row_subnet_id_ret, col_subnet_id_ret, subnet_id);
    createMatrixOfDoubleFrom_int(6, row_layer_id_ret, col_layer_id_ret, layer_id);
    strings_act_func_ret = (char**) malloc(row_act_func_ret * col_act_func_ret * sizeof(char*));
    for(cnt_act_func_ret = 0; cnt_act_func_ret < row_act_func_ret * col_act_func_ret ; ++cnt_act_func_ret) strings_act_func_ret[cnt_act_func_ret] = act_func[cnt_act_func_ret];
    createMatrixOfString(7, row_act_func_ret, col_act_func_ret, strings_act_func_ret);
    strings_out_func_ret = (char**) malloc(row_out_func_ret * col_out_func_ret * sizeof(char*));
    for(cnt_out_func_ret = 0; cnt_out_func_ret < row_out_func_ret * col_out_func_ret ; ++cnt_out_func_ret) strings_out_func_ret[cnt_out_func_ret] = out_func[cnt_out_func_ret];
    createMatrixOfString(8, row_out_func_ret, col_out_func_ret, strings_out_func_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;
    LhsVar(3) = 4;
    LhsVar(4) = 5;
    LhsVar(5) = 6;
    LhsVar(6) = 7;
    LhsVar(7) = 8;

    /*** Postrequisites return ***/
    free(strings_act_func_ret);
    free(strings_out_func_ret);

    /*** Memory free ***/
    free(act);
    free(bias);
    free(io_type);
    free(subnet_id);
    free(layer_id);
    free(act_func);
    free(out_func);


    return 0;
}


int sci_ann_set_unit_defaults(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    double *var_act;
    double *var_bias;
    int *var_io_type;
    int *var_subnet_id;
    int *var_layer_id;
    char *var_act_func;
    char *var_out_func;

    /*** Dimension parameters ***/
    int res_network;
    int row_act = 1, col_act = 1;
    int row_bias = 1, col_bias = 1;
    int row_io_type = 1, col_io_type = 1;
    int row_subnet_id = 1, col_subnet_id = 1;
    int row_layer_id = 1, col_layer_id = 1;
    int row_act_func = 1, col_act_func = 1;
    int row_out_func = 1, col_out_func = 1;

    /*** Address parameters ***/
    int *ptr_act = NULL;
    int *ptr_bias = NULL;
    int *ptr_io_type = NULL;
    int *ptr_subnet_id = NULL;
    int *ptr_layer_id = NULL;
    int *ptr_act_func = NULL;
    int cnt_act_func;
    int *lengths_act_func = NULL;
    char **strings_act_func = NULL;
    int *ptr_out_func = NULL;
    int cnt_out_func;
    int *lengths_out_func = NULL;
    char **strings_out_func = NULL;

    /*** Source parameters ***/
    int sci;
    double act;
    double bias;
    int io_type;
    int subnet_id;
    int layer_id;
    char* act_func;
    char* out_func;

    /*** Check for number of parameters ***/
    CheckRhs(8, 8);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_act);
    getVarAddressFromPosition(3, &ptr_bias);
    getVarAddressFromPosition(4, &ptr_io_type);
    getVarAddressFromPosition(5, &ptr_subnet_id);
    getVarAddressFromPosition(6, &ptr_layer_id);
    getVarAddressFromPosition(7, &ptr_act_func);
    getVarAddressFromPosition(8, &ptr_out_func);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_act, &row_act, &col_act);
    getVarDimension(ptr_bias, &row_bias, &col_bias);
    getVarDimension(ptr_io_type, &row_io_type, &col_io_type);
    getVarDimension(ptr_subnet_id, &row_subnet_id, &col_subnet_id);
    getVarDimension(ptr_layer_id, &row_layer_id, &col_layer_id);
    getVarDimension(ptr_act_func, &row_act_func, &col_act_func);
    getVarDimension(ptr_out_func, &row_out_func, &col_out_func);

    /*** Prerequisites destination ***/
    lengths_act_func = (int*) malloc(row_act_func * col_act_func * sizeof(int));
    getMatrixOfString(ptr_act_func, &row_act_func, &col_act_func, lengths_act_func, NULL);
    strings_act_func = (char**) malloc(row_act_func * col_act_func * sizeof(char*));
    for(cnt_act_func = 0; cnt_act_func < row_act_func * col_act_func ; ++cnt_act_func) strings_act_func[cnt_act_func] = (char*) malloc((lengths_act_func[cnt_act_func] + 1) * sizeof(char));
    lengths_out_func = (int*) malloc(row_out_func * col_out_func * sizeof(int));
    getMatrixOfString(ptr_out_func, &row_out_func, &col_out_func, lengths_out_func, NULL);
    strings_out_func = (char**) malloc(row_out_func * col_out_func * sizeof(char*));
    for(cnt_out_func = 0; cnt_out_func < row_out_func * col_out_func ; ++cnt_out_func) strings_out_func[cnt_out_func] = (char*) malloc((lengths_out_func[cnt_out_func] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfDouble(ptr_act, &row_act, &col_act, &var_act);
    getMatrixOfDouble(ptr_bias, &row_bias, &col_bias, &var_bias);
    getMatrixFromDoubleTo_int(ptr_io_type, &row_io_type, &col_io_type, &var_io_type);
    getMatrixFromDoubleTo_int(ptr_subnet_id, &row_subnet_id, &col_subnet_id, &var_subnet_id);
    getMatrixFromDoubleTo_int(ptr_layer_id, &row_layer_id, &col_layer_id, &var_layer_id);
    getMatrixOfString(ptr_act_func, &row_act_func, &col_act_func, lengths_act_func, strings_act_func);
    getMatrixOfString(ptr_out_func, &row_out_func, &col_out_func, lengths_out_func, strings_out_func);

    /*** Parameter copy ***/
    act = *var_act;
    bias = *var_bias;
    io_type = *var_io_type;
    subnet_id = *var_subnet_id;
    layer_id = *var_layer_id;
    act_func = strings_act_func[0];
    out_func = strings_out_func[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUnitDefaults(act, bias, io_type, subnet_id, layer_id, act_func, out_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_act_func = 0; cnt_act_func < row_act_func * col_act_func ; ++cnt_act_func) free(strings_act_func[cnt_act_func]);
    free(strings_act_func);
    free(lengths_act_func);
    for(cnt_out_func = 0; cnt_out_func < row_out_func * col_out_func ; ++cnt_out_func) free(strings_out_func[cnt_out_func]);
    free(strings_out_func);
    free(lengths_out_func);


    return 0;
}


int sci_ann_reset_net(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;

    /*** Dimension parameters ***/
    int res_network;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_resetNet();

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_set_seed_no(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    long int *var_seed;

    /*** Dimension parameters ***/
    int res_network;
    int row_seed = 1, col_seed = 1;

    /*** Address parameters ***/
    int *ptr_seed = NULL;

    /*** Source parameters ***/
    long int seed;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_seed);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_seed, &row_seed, &col_seed);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_long_int(ptr_seed, &row_seed, &col_seed, &var_seed);

    /*** Parameter copy ***/
    seed = *var_seed;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_setSeedNo(seed);

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_get_no_of_input_units(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNoOfInputUnits();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_no_of_output_units(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNoOfOutputUnits();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_no_of_special_input_units(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNoOfSpecialInputUnits();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_get_no_of_special_output_units(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNoOfSpecialOutputUnits();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}



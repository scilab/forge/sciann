#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


bool krui_getFirstSymbolTableEntry(char**, int*);
bool krui_getNextSymbolTableEntry(char**, int*);
bool krui_symbolSearch(char*, int);


int sci_ann_get_first_symbol_table_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;
    char *var_symbol_name_ret;
    int *var_symbol_type_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_symbol_name = 1, col_symbol_name = 1;
    int row_symbol_type = 1, col_symbol_type = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_symbol_name_ret = 1, col_symbol_name_ret = 1;
    int row_symbol_type_ret = 1, col_symbol_type_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int *ptr_symbol_name_ret = NULL;
    int cnt_symbol_name_ret;
    int *lengths_symbol_name_ret = NULL;
    char **strings_symbol_name_ret = NULL;
    int *ptr_symbol_type_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char** symbol_name;
    int* symbol_type;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(3, 3);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    symbol_name = (char**) malloc(sizeof(char*));
    symbol_type = (int*) malloc(row_symbol_type * col_symbol_type * sizeof(int));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFirstSymbolTableEntry(symbol_name, symbol_type);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    for(cnt_symbol_name_ret = 0; cnt_symbol_name_ret < row_symbol_name_ret * col_symbol_name_ret ; ++cnt_symbol_name_ret) if(symbol_name[cnt_symbol_name_ret] == NULL) { Scierror(999, "symbol_name is NULL"); return -1;}

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);
    strings_symbol_name_ret = (char**) malloc(row_symbol_name_ret * col_symbol_name_ret * sizeof(char*));
    for(cnt_symbol_name_ret = 0; cnt_symbol_name_ret < row_symbol_name_ret * col_symbol_name_ret ; ++cnt_symbol_name_ret) strings_symbol_name_ret[cnt_symbol_name_ret] = symbol_name[cnt_symbol_name_ret];
    createMatrixOfString(3, row_symbol_name_ret, col_symbol_name_ret, strings_symbol_name_ret);
    createMatrixOfDoubleFrom_int(4, row_symbol_type_ret, col_symbol_type_ret, symbol_type);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;
    LhsVar(3) = 4;

    /*** Postrequisites return ***/
    free(strings_symbol_name_ret);

    /*** Memory free ***/
    free(symbol_name);
    free(symbol_type);


    return 0;
}


int sci_ann_get_next_symbol_table_entry(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;
    char *var_symbol_name_ret;
    int *var_symbol_type_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_symbol_name = 1, col_symbol_name = 1;
    int row_symbol_type = 1, col_symbol_type = 1;
    int row_sci_ret = 1, col_sci_ret = 1;
    int row_symbol_name_ret = 1, col_symbol_name_ret = 1;
    int row_symbol_type_ret = 1, col_symbol_type_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int *ptr_symbol_name_ret = NULL;
    int cnt_symbol_name_ret;
    int *lengths_symbol_name_ret = NULL;
    char **strings_symbol_name_ret = NULL;
    int *ptr_symbol_type_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char** symbol_name;
    int* symbol_type;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(3, 3);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    symbol_name = (char**) malloc(sizeof(char*));
    symbol_type = (int*) malloc(row_symbol_type * col_symbol_type * sizeof(int));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getNextSymbolTableEntry(symbol_name, symbol_type);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    for(cnt_symbol_name_ret = 0; cnt_symbol_name_ret < row_symbol_name_ret * col_symbol_name_ret ; ++cnt_symbol_name_ret) if(symbol_name[cnt_symbol_name_ret] == NULL) { Scierror(999, "symbol_name is NULL"); return -1;}

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);
    strings_symbol_name_ret = (char**) malloc(row_symbol_name_ret * col_symbol_name_ret * sizeof(char*));
    for(cnt_symbol_name_ret = 0; cnt_symbol_name_ret < row_symbol_name_ret * col_symbol_name_ret ; ++cnt_symbol_name_ret) strings_symbol_name_ret[cnt_symbol_name_ret] = symbol_name[cnt_symbol_name_ret];
    createMatrixOfString(3, row_symbol_name_ret, col_symbol_name_ret, strings_symbol_name_ret);
    createMatrixOfDoubleFrom_int(4, row_symbol_type_ret, col_symbol_type_ret, symbol_type);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;
    LhsVar(3) = 4;

    /*** Postrequisites return ***/
    free(strings_symbol_name_ret);

    /*** Memory free ***/
    free(symbol_name);
    free(symbol_type);


    return 0;
}


int sci_ann_symbol_search(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_symbol;
    int *var_symbol_type;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_symbol = 1, col_symbol = 1;
    int row_symbol_type = 1, col_symbol_type = 1;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_symbol = NULL;
    int cnt_symbol;
    int *lengths_symbol = NULL;
    char **strings_symbol = NULL;
    int *ptr_symbol_type = NULL;
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;
    char* symbol;
    int symbol_type;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_symbol);
    getVarAddressFromPosition(3, &ptr_symbol_type);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_symbol, &row_symbol, &col_symbol);
    getVarDimension(ptr_symbol_type, &row_symbol_type, &col_symbol_type);

    /*** Prerequisites destination ***/
    lengths_symbol = (int*) malloc(row_symbol * col_symbol * sizeof(int));
    getMatrixOfString(ptr_symbol, &row_symbol, &col_symbol, lengths_symbol, NULL);
    strings_symbol = (char**) malloc(row_symbol * col_symbol * sizeof(char*));
    for(cnt_symbol = 0; cnt_symbol < row_symbol * col_symbol ; ++cnt_symbol) strings_symbol[cnt_symbol] = (char*) malloc((lengths_symbol[cnt_symbol] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_symbol, &row_symbol, &col_symbol, lengths_symbol, strings_symbol);
    getMatrixFromDoubleTo_int(ptr_symbol_type, &row_symbol_type, &col_symbol_type, &var_symbol_type);

    /*** Parameter copy ***/
    symbol = strings_symbol[0];
    symbol_type = *var_symbol_type;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_symbolSearch(symbol, symbol_type);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(4, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;

    /*** Postrequisites destination ***/
    for(cnt_symbol = 0; cnt_symbol < row_symbol * col_symbol ; ++cnt_symbol) free(strings_symbol[cnt_symbol]);
    free(strings_symbol);
    free(lengths_symbol);


    return 0;
}



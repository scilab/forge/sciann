#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


char* krui_getLearnFunc();
krui_err krui_setLearnFunc(char*);
int krui_checkPruning();
krui_err krui_learnAllPatterns(float*, int, float**, int*);
krui_err krui_learnSinglePattern(int, float*, int, float**, int*);
krui_err krui_learnAllPatternsFF(float*, int, float**, int*);
krui_err krui_learnSinglePatternFF(int, float*, int, float**, int*);
char* krui_getPrunFunc();
krui_err krui_setPrunFunc(char*);
char* krui_getFFLearnFunc();
krui_err krui_setFFLearnFunc(char*);


int sci_ann_get_learn_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getLearnFunc();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_learn_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_learning_func;

    /*** Dimension parameters ***/
    int res_network;
    int row_learning_func = 1, col_learning_func = 1;

    /*** Address parameters ***/
    int *ptr_learning_func = NULL;
    int cnt_learning_func;
    int *lengths_learning_func = NULL;
    char **strings_learning_func = NULL;

    /*** Source parameters ***/
    int sci;
    char* learning_func;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_learning_func);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_learning_func, &row_learning_func, &col_learning_func);

    /*** Prerequisites destination ***/
    lengths_learning_func = (int*) malloc(row_learning_func * col_learning_func * sizeof(int));
    getMatrixOfString(ptr_learning_func, &row_learning_func, &col_learning_func, lengths_learning_func, NULL);
    strings_learning_func = (char**) malloc(row_learning_func * col_learning_func * sizeof(char*));
    for(cnt_learning_func = 0; cnt_learning_func < row_learning_func * col_learning_func ; ++cnt_learning_func) strings_learning_func[cnt_learning_func] = (char*) malloc((lengths_learning_func[cnt_learning_func] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_learning_func, &row_learning_func, &col_learning_func, lengths_learning_func, strings_learning_func);

    /*** Parameter copy ***/
    learning_func = strings_learning_func[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setLearnFunc(learning_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_learning_func = 0; cnt_learning_func < row_learning_func * col_learning_func ; ++cnt_learning_func) free(strings_learning_func[cnt_learning_func]);
    free(strings_learning_func);
    free(lengths_learning_func);


    return 0;
}


int sci_ann_check_pruning(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;

    /*** Source parameters ***/
    int sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_checkPruning();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_sci_ret, col_sci_ret, &sci);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;


    return 0;
}


int sci_ann_learn_all_patterns(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    float *var_parameter_in_array;
    float *var_parameter_out_array_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_parameter_in_array = 1, col_parameter_in_array = 1;
    int row_no_of_in_params = 1, col_no_of_in_params = 1;
    int row_parameter_out_array = 1, col_parameter_out_array = 1;
    int row_no_of_out_params = 1, col_no_of_out_params = 1;
    int row_parameter_out_array_ret = 1, col_parameter_out_array_ret = 1;
    int row_no_of_out_params_ret = 1, col_no_of_out_params_ret = 1;

    /*** Address parameters ***/
    int *ptr_parameter_in_array = NULL;
    int *ptr_parameter_out_array_ret = NULL;

    /*** Source parameters ***/
    int sci;
    float* parameter_in_array;
    int no_of_in_params;
    float** parameter_out_array;
    int* no_of_out_params;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_parameter_in_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_float(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array, &var_parameter_in_array);

    /*** Memory allocation ***/
    parameter_out_array = (float**) malloc(row_parameter_out_array * col_parameter_out_array * sizeof(float*));
    no_of_out_params = (int*) malloc(row_no_of_out_params * col_no_of_out_params * sizeof(int));

    /*** Parameter copy ***/
    parameter_in_array = var_parameter_in_array;
    no_of_in_params = col_parameter_in_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_learnAllPatterns(parameter_in_array, no_of_in_params, parameter_out_array, no_of_out_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Copy return parameters ***/
    col_parameter_out_array_ret = *no_of_out_params;

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_float(3, row_parameter_out_array_ret, col_parameter_out_array_ret, *parameter_out_array);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Memory free ***/
    free(parameter_out_array);
    free(no_of_out_params);


    return 0;
}


int sci_ann_learn_single_pattern(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_pattern_id;
    float *var_parameter_in_array;
    float *var_parameter_out_array_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_pattern_id = 1, col_pattern_id = 1;
    int row_parameter_in_array = 1, col_parameter_in_array = 1;
    int row_no_of_in_params = 1, col_no_of_in_params = 1;
    int row_parameter_out_array = 1, col_parameter_out_array = 1;
    int row_no_of_out_params = 1, col_no_of_out_params = 1;
    int row_parameter_out_array_ret = 1, col_parameter_out_array_ret = 1;
    int row_no_of_out_params_ret = 1, col_no_of_out_params_ret = 1;

    /*** Address parameters ***/
    int *ptr_pattern_id = NULL;
    int *ptr_parameter_in_array = NULL;
    int *ptr_parameter_out_array_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int pattern_id;
    float* parameter_in_array;
    int no_of_in_params;
    float** parameter_out_array;
    int* no_of_out_params;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_pattern_id);
    getVarAddressFromPosition(3, &ptr_parameter_in_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_pattern_id, &row_pattern_id, &col_pattern_id);
    getVarDimension(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_pattern_id, &row_pattern_id, &col_pattern_id, &var_pattern_id);
    getMatrixFromDoubleTo_float(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array, &var_parameter_in_array);

    /*** Memory allocation ***/
    parameter_out_array = (float**) malloc(row_parameter_out_array * col_parameter_out_array * sizeof(float*));
    no_of_out_params = (int*) malloc(row_no_of_out_params * col_no_of_out_params * sizeof(int));

    /*** Parameter copy ***/
    pattern_id = *var_pattern_id;
    parameter_in_array = var_parameter_in_array;
    no_of_in_params = col_parameter_in_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_learnSinglePattern(pattern_id, parameter_in_array, no_of_in_params, parameter_out_array, no_of_out_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Copy return parameters ***/
    col_parameter_out_array_ret = *no_of_out_params;

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_float(4, row_parameter_out_array_ret, col_parameter_out_array_ret, *parameter_out_array);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;

    /*** Memory free ***/
    free(parameter_out_array);
    free(no_of_out_params);


    return 0;
}


int sci_ann_learn_all_patterns_ff(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    float *var_parameter_in_array;
    float *var_parameter_out_array_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_parameter_in_array = 1, col_parameter_in_array = 1;
    int row_no_of_in_params = 1, col_no_of_in_params = 1;
    int row_parameter_out_array = 1, col_parameter_out_array = 1;
    int row_no_of_out_params = 1, col_no_of_out_params = 1;
    int row_parameter_out_array_ret = 1, col_parameter_out_array_ret = 1;
    int row_no_of_out_params_ret = 1, col_no_of_out_params_ret = 1;

    /*** Address parameters ***/
    int *ptr_parameter_in_array = NULL;
    int *ptr_parameter_out_array_ret = NULL;

    /*** Source parameters ***/
    int sci;
    float* parameter_in_array;
    int no_of_in_params;
    float** parameter_out_array;
    int* no_of_out_params;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_parameter_in_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_float(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array, &var_parameter_in_array);

    /*** Memory allocation ***/
    parameter_out_array = (float**) malloc(row_parameter_out_array * col_parameter_out_array * sizeof(float*));
    no_of_out_params = (int*) malloc(row_no_of_out_params * col_no_of_out_params * sizeof(int));

    /*** Parameter copy ***/
    parameter_in_array = var_parameter_in_array;
    no_of_in_params = col_parameter_in_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_learnAllPatternsFF(parameter_in_array, no_of_in_params, parameter_out_array, no_of_out_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Copy return parameters ***/
    col_parameter_out_array_ret = *no_of_out_params;

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_float(3, row_parameter_out_array_ret, col_parameter_out_array_ret, *parameter_out_array);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 3;

    /*** Memory free ***/
    free(parameter_out_array);
    free(no_of_out_params);


    return 0;
}


int sci_ann_learn_single_pattern_ff(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_pattern_id;
    float *var_parameter_in_array;
    float *var_parameter_out_array_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_pattern_id = 1, col_pattern_id = 1;
    int row_parameter_in_array = 1, col_parameter_in_array = 1;
    int row_no_of_in_params = 1, col_no_of_in_params = 1;
    int row_parameter_out_array = 1, col_parameter_out_array = 1;
    int row_no_of_out_params = 1, col_no_of_out_params = 1;
    int row_parameter_out_array_ret = 1, col_parameter_out_array_ret = 1;
    int row_no_of_out_params_ret = 1, col_no_of_out_params_ret = 1;

    /*** Address parameters ***/
    int *ptr_pattern_id = NULL;
    int *ptr_parameter_in_array = NULL;
    int *ptr_parameter_out_array_ret = NULL;

    /*** Source parameters ***/
    int sci;
    int pattern_id;
    float* parameter_in_array;
    int no_of_in_params;
    float** parameter_out_array;
    int* no_of_out_params;

    /*** Check for number of parameters ***/
    CheckRhs(3, 3);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_pattern_id);
    getVarAddressFromPosition(3, &ptr_parameter_in_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_pattern_id, &row_pattern_id, &col_pattern_id);
    getVarDimension(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_pattern_id, &row_pattern_id, &col_pattern_id, &var_pattern_id);
    getMatrixFromDoubleTo_float(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array, &var_parameter_in_array);

    /*** Memory allocation ***/
    parameter_out_array = (float**) malloc(row_parameter_out_array * col_parameter_out_array * sizeof(float*));
    no_of_out_params = (int*) malloc(row_no_of_out_params * col_no_of_out_params * sizeof(int));

    /*** Parameter copy ***/
    pattern_id = *var_pattern_id;
    parameter_in_array = var_parameter_in_array;
    no_of_in_params = col_parameter_in_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_learnSinglePatternFF(pattern_id, parameter_in_array, no_of_in_params, parameter_out_array, no_of_out_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Copy return parameters ***/
    col_parameter_out_array_ret = *no_of_out_params;

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_float(4, row_parameter_out_array_ret, col_parameter_out_array_ret, *parameter_out_array);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 4;

    /*** Memory free ***/
    free(parameter_out_array);
    free(no_of_out_params);


    return 0;
}


int sci_ann_get_prun_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getPrunFunc();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_prun_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_pruning_func;

    /*** Dimension parameters ***/
    int res_network;
    int row_pruning_func = 1, col_pruning_func = 1;

    /*** Address parameters ***/
    int *ptr_pruning_func = NULL;
    int cnt_pruning_func;
    int *lengths_pruning_func = NULL;
    char **strings_pruning_func = NULL;

    /*** Source parameters ***/
    int sci;
    char* pruning_func;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_pruning_func);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_pruning_func, &row_pruning_func, &col_pruning_func);

    /*** Prerequisites destination ***/
    lengths_pruning_func = (int*) malloc(row_pruning_func * col_pruning_func * sizeof(int));
    getMatrixOfString(ptr_pruning_func, &row_pruning_func, &col_pruning_func, lengths_pruning_func, NULL);
    strings_pruning_func = (char**) malloc(row_pruning_func * col_pruning_func * sizeof(char*));
    for(cnt_pruning_func = 0; cnt_pruning_func < row_pruning_func * col_pruning_func ; ++cnt_pruning_func) strings_pruning_func[cnt_pruning_func] = (char*) malloc((lengths_pruning_func[cnt_pruning_func] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_pruning_func, &row_pruning_func, &col_pruning_func, lengths_pruning_func, strings_pruning_func);

    /*** Parameter copy ***/
    pruning_func = strings_pruning_func[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setPrunFunc(pruning_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_pruning_func = 0; cnt_pruning_func < row_pruning_func * col_pruning_func ; ++cnt_pruning_func) free(strings_pruning_func[cnt_pruning_func]);
    free(strings_pruning_func);
    free(lengths_pruning_func);


    return 0;
}


int sci_ann_get_ff_learn_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getFFLearnFunc();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_ff_learn_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_ff_learning_func;

    /*** Dimension parameters ***/
    int res_network;
    int row_ff_learning_func = 1, col_ff_learning_func = 1;

    /*** Address parameters ***/
    int *ptr_ff_learning_func = NULL;
    int cnt_ff_learning_func;
    int *lengths_ff_learning_func = NULL;
    char **strings_ff_learning_func = NULL;

    /*** Source parameters ***/
    int sci;
    char* ff_learning_func;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_ff_learning_func);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_ff_learning_func, &row_ff_learning_func, &col_ff_learning_func);

    /*** Prerequisites destination ***/
    lengths_ff_learning_func = (int*) malloc(row_ff_learning_func * col_ff_learning_func * sizeof(int));
    getMatrixOfString(ptr_ff_learning_func, &row_ff_learning_func, &col_ff_learning_func, lengths_ff_learning_func, NULL);
    strings_ff_learning_func = (char**) malloc(row_ff_learning_func * col_ff_learning_func * sizeof(char*));
    for(cnt_ff_learning_func = 0; cnt_ff_learning_func < row_ff_learning_func * col_ff_learning_func ; ++cnt_ff_learning_func) strings_ff_learning_func[cnt_ff_learning_func] = (char*) malloc((lengths_ff_learning_func[cnt_ff_learning_func] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_ff_learning_func, &row_ff_learning_func, &col_ff_learning_func, lengths_ff_learning_func, strings_ff_learning_func);

    /*** Parameter copy ***/
    ff_learning_func = strings_ff_learning_func[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setFFLearnFunc(ff_learning_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_ff_learning_func = 0; cnt_ff_learning_func < row_ff_learning_func * col_ff_learning_func ; ++cnt_ff_learning_func) free(strings_ff_learning_func[cnt_ff_learning_func]);
    free(strings_ff_learning_func);
    free(lengths_ff_learning_func);


    return 0;
}



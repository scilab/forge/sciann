#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


krui_err krui_updateSingleUnit(int);
char* krui_getUpdateFunc();
krui_err krui_setUpdateFunc(char*);
krui_err krui_updateNet(float*, int);


int sci_ann_update_single_unit(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_id;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_id = 1, col_unit_id = 1;

    /*** Address parameters ***/
    int *ptr_unit_id = NULL;

    /*** Source parameters ***/
    int sci;
    int unit_id;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_unit_id);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_unit_id, &row_unit_id, &col_unit_id);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_int(ptr_unit_id, &row_unit_id, &col_unit_id, &var_unit_id);

    /*** Parameter copy ***/
    unit_id = *var_unit_id;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_updateSingleUnit(unit_id);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}


int sci_ann_get_update_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_sci_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_sci_ret = 1, col_sci_ret = 1;

    /*** Address parameters ***/
    int *ptr_sci_ret = NULL;
    int cnt_sci_ret;
    int *lengths_sci_ret = NULL;
    char **strings_sci_ret = NULL;

    /*** Source parameters ***/
    char* sci;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(1, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_getUpdateFunc();

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** String check ***/
    if(sci == NULL) { Scierror(999, "sci is NULL"); return -1; }

    /*** Create return parameters ***/
    strings_sci_ret = (char**) malloc(row_sci_ret * col_sci_ret * sizeof(char*));
    strings_sci_ret[0] = sci;
    createMatrixOfString(2, row_sci_ret, col_sci_ret, strings_sci_ret);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;

    /*** Postrequisites return ***/
    free(strings_sci_ret);


    return 0;
}


int sci_ann_set_update_func(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    char *var_update_func;

    /*** Dimension parameters ***/
    int res_network;
    int row_update_func = 1, col_update_func = 1;

    /*** Address parameters ***/
    int *ptr_update_func = NULL;
    int cnt_update_func;
    int *lengths_update_func = NULL;
    char **strings_update_func = NULL;

    /*** Source parameters ***/
    int sci;
    char* update_func;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_update_func);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_update_func, &row_update_func, &col_update_func);

    /*** Prerequisites destination ***/
    lengths_update_func = (int*) malloc(row_update_func * col_update_func * sizeof(int));
    getMatrixOfString(ptr_update_func, &row_update_func, &col_update_func, lengths_update_func, NULL);
    strings_update_func = (char**) malloc(row_update_func * col_update_func * sizeof(char*));
    for(cnt_update_func = 0; cnt_update_func < row_update_func * col_update_func ; ++cnt_update_func) strings_update_func[cnt_update_func] = (char*) malloc((lengths_update_func[cnt_update_func] + 1) * sizeof(char));

    /*** Getting parameters from stack ***/
    getMatrixOfString(ptr_update_func, &row_update_func, &col_update_func, lengths_update_func, strings_update_func);

    /*** Parameter copy ***/
    update_func = strings_update_func[0];

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_setUpdateFunc(update_func);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }

    /*** Postrequisites destination ***/
    for(cnt_update_func = 0; cnt_update_func < row_update_func * col_update_func ; ++cnt_update_func) free(strings_update_func[cnt_update_func]);
    free(strings_update_func);
    free(lengths_update_func);


    return 0;
}


int sci_ann_update_net(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    float *var_parameter_in_array;

    /*** Dimension parameters ***/
    int res_network;
    int row_parameter_in_array = 1, col_parameter_in_array = 1;
    int row_no_of_in_params = 1, col_no_of_in_params = 1;

    /*** Address parameters ***/
    int *ptr_parameter_in_array = NULL;

    /*** Source parameters ***/
    int sci;
    float* parameter_in_array;
    int no_of_in_params;

    /*** Check for number of parameters ***/
    CheckRhs(2, 2);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);
    getVarAddressFromPosition(2, &ptr_parameter_in_array);

    /*** Getting the dimensions of the parameters ***/
    getVarDimension(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array);

    /*** Getting parameters from stack ***/
    getMatrixFromDoubleTo_float(ptr_parameter_in_array, &row_parameter_in_array, &col_parameter_in_array, &var_parameter_in_array);

    /*** Parameter copy ***/
    parameter_in_array = var_parameter_in_array;
    no_of_in_params = col_parameter_in_array;

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    sci = krui_updateNet(parameter_in_array, no_of_in_params);

    /*** Hook functions end ***/
    krm_getNet(var_network);
    if(sci != KRERR_NO_ERROR) { Scierror(999, krui_error(sci)); return -1; }


    return 0;
}



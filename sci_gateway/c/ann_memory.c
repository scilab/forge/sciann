#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"


void krui_deleteNet();
void krui_getMemoryManagerInfo(int*, int*, int*, int*, int*, int*);


int sci_ann_delete_net(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;

    /*** Dimension parameters ***/
    int res_network;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(0, 1);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_deleteNet();

    /*** Hook functions end ***/
    krm_getNet(var_network);


    return 0;
}


int sci_ann_get_memory_manager_info(char *fname)
{
    /*** Destination parameters ***/
    memNet *var_network;
    int *var_unit_bytes_ret;
    int *var_site_bytes_ret;
    int *var_link_bytes_ret;
    int *var_n_table_bytes_ret;
    int *var_s_table_bytes_ret;
    int *var_f_table_bytes_ret;

    /*** Dimension parameters ***/
    int res_network;
    int row_unit_bytes = 1, col_unit_bytes = 1;
    int row_site_bytes = 1, col_site_bytes = 1;
    int row_link_bytes = 1, col_link_bytes = 1;
    int row_n_table_bytes = 1, col_n_table_bytes = 1;
    int row_s_table_bytes = 1, col_s_table_bytes = 1;
    int row_f_table_bytes = 1, col_f_table_bytes = 1;
    int row_unit_bytes_ret = 1, col_unit_bytes_ret = 1;
    int row_site_bytes_ret = 1, col_site_bytes_ret = 1;
    int row_link_bytes_ret = 1, col_link_bytes_ret = 1;
    int row_n_table_bytes_ret = 1, col_n_table_bytes_ret = 1;
    int row_s_table_bytes_ret = 1, col_s_table_bytes_ret = 1;
    int row_f_table_bytes_ret = 1, col_f_table_bytes_ret = 1;

    /*** Address parameters ***/
    int *ptr_unit_bytes_ret = NULL;
    int *ptr_site_bytes_ret = NULL;
    int *ptr_link_bytes_ret = NULL;
    int *ptr_n_table_bytes_ret = NULL;
    int *ptr_s_table_bytes_ret = NULL;
    int *ptr_f_table_bytes_ret = NULL;

    /*** Source parameters ***/
    int* unit_bytes;
    int* site_bytes;
    int* link_bytes;
    int* n_table_bytes;
    int* s_table_bytes;
    int* f_table_bytes;

    /*** Check for number of parameters ***/
    CheckRhs(1, 1);
    CheckLhs(6, 6);

    /*** Getting the address of the parameters ***/
    var_network = stoc_memNet(1, &res_network);

    /*** Memory allocation ***/
    unit_bytes = (int*) malloc(row_unit_bytes * col_unit_bytes * sizeof(int));
    site_bytes = (int*) malloc(row_site_bytes * col_site_bytes * sizeof(int));
    link_bytes = (int*) malloc(row_link_bytes * col_link_bytes * sizeof(int));
    n_table_bytes = (int*) malloc(row_n_table_bytes * col_n_table_bytes * sizeof(int));
    s_table_bytes = (int*) malloc(row_s_table_bytes * col_s_table_bytes * sizeof(int));
    f_table_bytes = (int*) malloc(row_f_table_bytes * col_f_table_bytes * sizeof(int));

    /*** Hook functions start ***/
    krm_putNet(var_network);


    // *** Function call
    krui_getMemoryManagerInfo(unit_bytes, site_bytes, link_bytes, n_table_bytes, s_table_bytes, f_table_bytes);

    /*** Hook functions end ***/
    krm_getNet(var_network);

    /*** Create return parameters ***/
    createMatrixOfDoubleFrom_int(2, row_unit_bytes_ret, col_unit_bytes_ret, unit_bytes);
    createMatrixOfDoubleFrom_int(3, row_site_bytes_ret, col_site_bytes_ret, site_bytes);
    createMatrixOfDoubleFrom_int(4, row_link_bytes_ret, col_link_bytes_ret, link_bytes);
    createMatrixOfDoubleFrom_int(5, row_n_table_bytes_ret, col_n_table_bytes_ret, n_table_bytes);
    createMatrixOfDoubleFrom_int(6, row_s_table_bytes_ret, col_s_table_bytes_ret, s_table_bytes);
    createMatrixOfDoubleFrom_int(7, row_f_table_bytes_ret, col_f_table_bytes_ret, f_table_bytes);

    /*** Set return parameters on stack ***/
    LhsVar(1) = 2;
    LhsVar(2) = 3;
    LhsVar(3) = 4;
    LhsVar(4) = 5;
    LhsVar(5) = 6;
    LhsVar(6) = 7;

    /*** Memory free ***/
    free(unit_bytes);
    free(site_bytes);
    free(link_bytes);
    free(n_table_bytes);
    free(s_table_bytes);
    free(f_table_bytes);


    return 0;
}



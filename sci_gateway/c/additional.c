#include "stack-c.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"

#include "helper_user_types.h"
#include "helper_type_converters.h"

#include "config.h"
#include "glob_typ.h"
#include "kr_ui.h"
#include "kr_typ.h"
#include "kr_mem.h"
#include "enzo_mem_typ.h"



int sci_ann_create_network( char *fname )
{
    int res;
    memNet *ann = (memNet *) malloc(sizeof(memNet));

    CheckRhs(0,1) ;
    CheckLhs(1,1) ;   

    krm_getNet( ann ); 

    res = ctos_memNet(1, ann);
    if (res) return res;
    if ( ann == NULL )
    {
        Scierror(999,"Problem while creating the SciANN structure\n");
        return 0;
    }

    LhsVar(1) = 1;
    return 0;
}

